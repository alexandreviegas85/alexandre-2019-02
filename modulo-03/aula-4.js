/* class Turma {
    constructor(ano) {
        this.ano = ano;
    }

    apresentarAno() {
        console.log(`Essa turma é do ano de: ${this.ano}`);
    }

    // só acessivel dentro da class
    static info() {
        console.log(`Testando informações`)
    }

    get anoTurma() {
        console.log(this.ano)
    }

    set localMetodo(localizacao) {
        this.localTurma = localizacao
    }
}

 const vemSer = new Turma("2019/02")

vemSer.apresentarAno()
vemSer.anoTurma // get e set não precisa de () na chamada
vemSer.local = "DBC"
console.log(vemSer.localTurma) 

class VemSer extends Turma {
    constructor(ano, local, qtdAlunos) {
        super(ano)
        this.local = local
        this.qtdAlunos = qtdAlunos
    }

    descricao(){
        console.log(`Turma do Vem Ser ano ${this.ano} realizado na ${this.local}, quantidade de alunos ${this.qtdAlunos}`)
    }
}

const vemSer = new VemSer("2019/02", "DBC", 17)
vemSer.descricao() */

/* let defer = new Promise((resolve, reject) => {
    setTimeout(() => {
        if (true) {
            resolve("foi resolvido")
        } else
            reject('Erro')
    },3000)
})


defer
    .then((data) => {
        console.log(data)
        return 'novo resultado'
    })
    .then((data) => console.log(data))
    .catch((erro) => console.log(erro)) */

/* let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/`)

//pending
//resolved
//rejected
pokemon
.then(data => data.json())
    .then(data => {console.log(data.results)}) */

const valor1 = new Promise((resolve, reject) => {
    setTimeout(()=>{
        resolve({valorAtual: '1'})

    },4000)
})

const valor2 = new Promise((resolve, reject) => {
    resolve({valorAtual: '2'})
})

/* Promise
    .all([valor1,valor2])
    .then(resposta => {
        console.log(resposta)
    }) */
   
    Promise
    .race([valor1,valor2]) // tras a api que responder primeiro
    .then(resposta => {
        console.log(resposta)
    })