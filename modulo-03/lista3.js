function cardapioIFood(veggie = true, comLactose = false) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]

  if (comLactose) {
    cardapio.push('pastel de queijo')
  }

  cardapio = [...cardapio, 'pastel de carne',
    'empada de legumes marabijosa'];
  /* cardapio = cardapio.concat( [
    'pastel de carne',
    'empada de legumes marabijosa'
  ] ) */

  if (veggie) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)

    cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1)
    cardapio.splice(cardapio.indexOf('pastel de carne'), 1)
  }

  //let resultado = cardapio.filter(alimento => alimento === 'cuca de uva').map(alimento => alimento.toUpperCase());

  let resultado = cardapio.map(alimento => alimento.toUpperCase());

 //console.log(resultado)

 // return resultado;
}

//cardapioIFood(true, true) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]


//let inputTeste = document.getElementById('campoTeste')
//inputTeste.addEventListener('blur', cardapioIFood(true, true));


String.prototype.correr = function(upper = false){
  let texto = `${this} estou correndo` ;
  return upper ? texto.toUpperCase() : texto;
}


console.log("eu".correr())
console.log("eu".correr())
