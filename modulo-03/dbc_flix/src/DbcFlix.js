import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import './css/DbcFlix.css';
import ListaSeries from './models/ListaSeries';
import Menu from './components/Menu';
import Titles from './components/Titles';

export default class DbcFlix extends Component {
  constructor(props) {
    super(props)
    this.ListaSeries = new ListaSeries()
    this.exibirTodas = this.exibirTodas.bind(this)
    this.buscarPorTitulo = this.buscarPorTitulo.bind(this)
    this.exibirInvalidas = this.exibirInvalidas.bind(this)
    this.buscarPorAno = this.buscarPorAno.bind(this)
    this.buscarPorGenero = this.buscarPorGenero.bind(this)
    this.state = {
      series: this.ListaSeries.todasSeries
    }
  }

  exibirInvalidas() {
    const invalidas = Array.prototype.invalidas()
    console.log(invalidas)
    this.setState({ series: invalidas })
  }

  exibirTodas() {
    const todasSeries = this.ListaSeries.todasSeries
    console.log(todasSeries)
    this.setState({ series: todasSeries })
  }

  buscarPorTitulo(event) {
    const listaPorTitulo = Array.prototype.queroTitulo(event.target.value)
    console.log(listaPorTitulo)
    this.setState({ series: listaPorTitulo })
  }

  buscarPorAno(event) {
    const listaPorAno = Array.prototype.filtrarPorAno(event.target.value)
    console.log(listaPorAno)
    this.setState({ series: listaPorAno })
  }

  buscarPorGenero(event) {
    const listaPorGenero = Array.prototype.queroGenero(event.target.value)
    console.log(listaPorGenero)
    this.setState({ series: listaPorGenero })
  }


  //RENDERIZACAO
  render() {
    return (
      <div className="App-dbcFlix">
        <div className='header'>
          <Link className="logoLink button" to='/'>Home</Link>
          <Link className='button' to='/blackMirror'>BlackMirror</Link>
          <h1 className='nameHeader'>DBCFLIX</h1>
        </div>
        <div>
          <Menu listaSeries={this.ListaSeries.todasSeries}
            buscarPorTitulo={this.buscarPorTitulo}
            exibirTodas={this.exibirTodas}
            exibirInvalidas={this.exibirInvalidas}
            buscarPorAno={this.buscarPorAno}
            buscarPorGenero={this.buscarPorGenero}
          />
        </div>
        <div className="titles">
          <Titles listaSeries={this.state.series} />
        </div>
        {/* <Link to="/teste">teste</Link> */}
      </div>
    );
  }
}


