import React from 'react';

 const MensagemFlash = props => {
   
    return props.exibirMenssagem ? 
     (
        <div>
           {props.notaValida ? (
            <div>
               <h4 className='msgNotaVerde elementToFadeInAndOut'>Nota registrada com sucesso!</h4>
            </div>
           ):
           (
            <div>
               <h4 className='msgNotaVermelha elementToFadeInAndOut'>Nota Inválida, informar uma nota entre 1 e 5.</h4>
            </div>
           )}
        </div>
    ):
    (
        <div></div>
    )
}
export default MensagemFlash