import React from 'react';

const Titles = props => {
    return (
        <React.Fragment>
            <div className="container">
                <div className='row'>
                        {props.listaSeries.map((serie) => {
                            return (
                            <>
                                <div className="col col-sm-50">
                                    <div className="poster">
                                        <p>{serie.titulo}</p>
                                        <p>{serie.anoEstreia}</p>
                                        <p>Temporadas: {serie.temporadas}</p>
                                        <p>Episódios: {serie.numeroEpisodios}</p>
                                        <p>Distribuidora: {serie.distribuidora}</p>
                                        <p>Diretor: {serie.diretor}</p>
                                        <p>Gênero: {serie.genero.map(element => {
                                            return <> {element} </>
                                        })}</p>
                                        <p>Elenco: {serie.elenco.map(element => {
                                            return <>{element} </>
                                        })}</p>
                                    </div>
                                </div>
                                
                            </>)
                        })}
                </div>
            </div>
        </React.Fragment>
    )
}
export default Titles;