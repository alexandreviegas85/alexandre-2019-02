import React from 'react';

const Menu = props =>{
    return(
        <React.Fragment>
            <div className='stlMenu'>
            <ul className='menuLista'>
                <li>
                    <button onClick={ () => props.exibirTodas() }>Todas Séries</button>
                </li>
                <li>
                    <button onClick={ () => props.exibirInvalidas()} >Inválidas</button>
                </li>
                <li className="input-search">
                    <input placeholder="Genero" type="text" onChange={ props.buscarPorGenero }/>
                </li>  
                <li className="input-search">
                    <input placeholder="Título" type="text" onChange={ props.buscarPorTitulo }/>
                </li>    
                <li className="input-search">
                    <input placeholder="Ano" type="text" onChange={ props.buscarPorAno }/>
                </li>    
            </ul>
            </div>
            
        </React.Fragment>
    )
}
export default Menu