import React, { Component } from 'react';
import * as axios from 'axios';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    trocaValoresState(evt) {
        const { name, value } = evt.target;
        this.setState(
            {
                [name]: value
            }
        )
    }

    logar(evt) {
        evt.preventDefault();
        const { email, password } = this.state
        if (email && password) {
            //executo a regra de email
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                password: this.state.password
            }).then(response =>{
                localStorage.setItem('Authorization', response.data.token);
                this.props.history.push('/')
                console.log(response)
            } )
        }
    }

render(){
    return (
        <React.Fragment>
            <h5>Logar</h5>
            <input type='text' name='email' id='nome' placeholder='Digite o username' onChange={this.trocaValoresState} />
            <input type='password' name='password' id='password' placeholder='Digite o password' onChange={this.trocaValoresState} />
            <button onClick={this.logar.bind(this)}>Logar</button>
        </React.Fragment>

    )

}
}