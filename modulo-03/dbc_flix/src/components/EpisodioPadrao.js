import React from 'react';
import SystemButton from './SystemButton'

const EpisodioPadrao = props => {
    const { episodio } = props
    return (
        <React.Fragment>
            <h1>{episodio.nome}</h1>
            <img className="imgEpisodio" src={episodio.thumbUrl} alt={episodio.nome}></img>
            <h3>Já assistido? {episodio.assistido ? 'Sim' : 'Não'}, {episodio.qtdVezesAssistido} vez(es)</h3>
            <p>Temp/Ep: {episodio.temporadaEpisodio}</p>
            <p>Duração: {episodio.duracaoEmMin} </p>
            <h4>{episodio.nota || 'sem nota'}</h4>
            <div className=''>
                <SystemButton cor='button' quandoClicar={props.sortearNoComp} texto='Próximo'/>
                <SystemButton cor='button' quandoClicar={props.marcarNoComp} texto='Já assisti'/>
               {/*  <button className='button' onClick={ props.sortearNoComp }>Próximo</button>
                <button className='button' onClick={ props.marcarNoComp }>Já assisti</button> */}
            </div>
        </React.Fragment>
    )
}
export default EpisodioPadrao