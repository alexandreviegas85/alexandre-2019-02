import React from 'react';

const MeuInputNumero = props => {
    return (
        <React.Fragment>
            {typeof props.msgSpan!== undefined ?<span>{props.msgSpan}</span>: "" }
            <input className="inputNota" type="number" placeholder={props.placeHolder} onBlur={props.registrarNota}></input>
        </React.Fragment>
    )
}
export default MeuInputNumero