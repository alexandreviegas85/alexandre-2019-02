import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Link} from 'react-router-dom'
import DbcFlix from './DbcFlix';
import BlackMirror from './BlackMirror';
import './css/App-home.css'
import Login from './components/Login'
import PrivateRoute from "./components/PrivateRoute"

export default class App extends Component { 
  render(){
    return (
      <div className="App-home header">
        <Router>
            <React.Fragment>
                <section className=''>
                    <PrivateRoute path="/" exact component={paginaInicial}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/dbcFlix" component={DbcFlix}/>
                    <Route path="/blackMirror" component={BlackMirror}/>
                </section>
            </React.Fragment>
        </Router>
      </div>
    );
  }
}

const paginaInicial = () => {
    return (
      <>
      <div className='App-home header'>
        <Link className='link button' to='/dbcFlix'>DbcFlix</Link>
        <Link className='link button' to='/blackMirror'>BlackMirror</Link>
      </div>
        
      </>
  )
}

