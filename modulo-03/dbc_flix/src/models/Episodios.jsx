export default class Episodios {
    constructor(nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido) {
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordemEpisodio = ordemEpisodio
        this.thumbUrl = thumbUrl
        this.qtdVezesAssistido = qtdVezesAssistido || 0
    }

    validarNota(nota){
        nota = parseInt(nota)
        return 1 <= nota && nota <=5
    }

    avaliar(nota) {
        this.nota = parseInt(nota)
    }

    get duracaoEmMin() {
        return `${this.duracao} min`
    }

    get temporadaEpisodio() {
        return `${this.temporada.toString().padStart(2, '0')}/${this.ordemEpisodio.toString().padStart(2, '0')}`
    }

  /*   get notaEpisodio(){
        return `${this.nome.toString()} nota: ${this.nota.toString()}.`
    } */
}