import Serie from "./Series";

export default class ListaSeries {
    constructor() {
        this.todos = [
            { titulo:"Stranger Things", anoEstreia:2016, diretor:["Matt Duffer","Ross Duffer"], genero :["Suspense","Ficcao Cientifica","Drama"], elenco :["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],temporadas:2,numeroEpisodios:17,distribuidora:"Netflix"},
            { titulo:"Game Of Thrones", anoEstreia:2011,diretor:["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"], genero:["Fantasia","Drama"], elenco:["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],temporadas:7,numeroEpisodios:67,distribuidora:"HBO"},
            { titulo:"The Walking Dead", anoEstreia:2010,diretor:["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"], genero:["Terror","Suspense","Apocalipse Zumbi"], elenco:["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],temporadas:9,numeroEpisodios:122,distribuidora:"AMC"},
            { titulo:"Band of Brothers", anoEstreia:20001,diretor:["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"], genero:["Guerra"], elenco:["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],temporadas:1,numeroEpisodios:10,distribuidora:"HBO"},
            { titulo:"The JS Mirror", anoEstreia:2017,diretor:["Lisandro","Jaime","Edgar"], genero:["Terror","Caos","JavaScript"], elenco:["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],temporadas:1,numeroEpisodios:40,distribuidora:"DBC"},
            { titulo:"10 Days Why", anoEstreia:2010,diretor:["Brendan Eich"], genero:["Caos","JavaScript"], elenco:["Brendan Eich","Bernardo Bosak"],temporadas:10,numeroEpisodios:10,distribuidora:"JS"},
            { titulo:"Mr. Robot", anoEstreia:2018,diretor:["Sam Esmail"], genero:["Drama","Techno Thriller","Psychological Thriller"], elenco:["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],temporadas:3,numeroEpisodios:32,distribuidora:"USA Network"},
            { titulo:"Narcos", anoEstreia:2015,diretor:["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"], genero:["Documentario","Crime","Drama"], elenco:["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],temporadas:3,numeroEpisodios:30,distribuidora:null},
            { titulo:"Westworld", anoEstreia:2016,diretor:["Athena Wickham"], genero:["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"], elenco:["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],temporadas:2,numeroEpisodios:20,distribuidora:"HBO"},
            { titulo:"Breaking Bad", anoEstreia:2008,diretor:["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"], genero:["Acao","Suspense","Drama","Crime","Humor Negro"], elenco:["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],temporadas:5,numeroEpisodios:62,distribuidora:"AMC"}
        ].map( e => new Serie(e.titulo, e.anoEstreia, e.diretor, e.genero, e.elenco, e.temporadas, e.numeroEpisodios, e.distribuidora));
    }
    
    get todasSeries() {
        return this.todos;
    }

}

// Filtrar séries Inválidas
Array.prototype.invalidas = function () {
    const series = new ListaSeries().todasSeries;
    let invalidas = []
  
    for (let i = 0; i < series.length; i++) {
      const isNulo = Object.values(series[i]).some(s => (s === null));
      if (series[i].anoEstreia > 2019 || isNulo) {
        invalidas.push(series[i])
      }
    }
    console.log(invalidas)
    return invalidas
  }
  
  //Filtrar séries a partir de um determinado ano
  Array.prototype.filtrarPorAno1 = function (ano) {
    const series = new ListaSeries().todasSeries;
    let arrayAno = []
  
    for (let i = 0; i < series.length; i++) {
      const retorno = Object.values(series[i]).some(s => (s >= ano))
      if (retorno) {
        arrayAno.push(series[i])
      }
    }
    //console.log(arrayAno)
    return arrayAno
  }

  Array.prototype.filtrarPorAno = function( ano ) {
    const series = new ListaSeries().todasSeries
    return series.filter( serie => serie.anoEstreia >= ano );
}
  
  //Filtrar nome do elenco
  Array.prototype.procurarPorNome = function (nome) {
    const series = new ListaSeries().todasSeries;
    for (let index = 0; index < series.length; index++) {
      for (let j = 0; j < series[index].elenco.length; j++) {
        if (series[index].elenco[j].match(nome)) {
          console.log(nome)
          return true
        }
      }
    }
    return false
  }

  
  //Função retorna média dos episódios de todas as séries do array
  Array.prototype.mediaEpisodios = function () {
    const series = new ListaSeries().todasSeries;
    let total = 0
    for (let i = 0; i < series.length; i++) {
      total += series[i].numeroEpisodios
    }
    let media = total / series.length
    Math.ceil(media)
    console.log(media)
    return media
  }
  
  //Retorna o valor total de gastos da série com salários
  Array.prototype.totalSalarios = function (indice) {
    const series = new ListaSeries().todasSeries;
    let bigBosses = 0
    let operarios = 0
    let total = 0
    for (let i = 0; i < series[indice].diretor.length; i++) {
      bigBosses += 100000
    }
    for (let j = 0; j < series[indice].elenco.length; j++) {
      operarios += 40000
    }
    total = operarios + bigBosses
    console.log(total)
    return `Total salário R$: ${total},00`
  }
  
  // Retorna séries por genero
  Array.prototype.queroGenero = function (genero) {
    const series = new ListaSeries().todasSeries;
    let generos = []
    for (let i = 0; i < series.length; i++) {
      for (let j = 0; j < series[i].genero.length; j++) {
        if (series[i].genero[j].match(genero)) {
          generos.push(series[i])
        }
      }
    }
    //console.log(generos)
    return generos
  }
    
  //Retorna séries por título
  Array.prototype.queroTitulo = function( titulo ) {
    const series = new ListaSeries().todasSeries
    return series.filter(serie => serie.titulo.toLowerCase().includes(titulo.toLowerCase()))
}

  //Função ordenar por ultimo nome
  function ordenaPorUltimoNome(itens) {
    return itens.sort(function (a, b) {
      a = a.split(' ')
      b = b.split(' ')
      if (a[a.length - 1] > b[b.length - 1]) {
      }
      if (a[a.length - 1] < b[b.length - 1]) {
        return -1;
      }
      return 0;
    });
  }
  
  //Função crédito episódios
  Array.prototype.creditos = function () {
    const series = new ListaSeries().todasSeries;
    let creditos = []
    for (let i = 0; i < series.length; i++) {
      creditos.push(`Titulo: ${series[i].titulo} Diretor(es): ${ordenaPorUltimoNome(series[i].diretor)} Elenco: ${ordenaPorUltimoNome(series[i].diretor)} /`)
    }
    console.log(creditos)
    return creditos.toString()
  }