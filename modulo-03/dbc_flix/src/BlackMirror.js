
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import * as axios from 'axios';
import EpisodioPadrao from './components/EpisodioPadrao'
import ListaEpisodios from './models/ListaEpisodios'
import './css/BlackMirror.css'
import MensagemFlash from './MensagemFlash';
import MeuInputNumero from './components/MeuInputNumero';
import ListaDeAvaliados from './components/ListaDeAvaliacoes';

export default class BlackMirror extends Component {
  constructor(props) {
    super(props)
    this.avaliados = []
    this.listaEpisodios = new ListaEpisodios()
    this.sortear = this.sortear.bind(this)
    this.registrarNota = this.registrarNota.bind(this)
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMenssagem: false,
      notaValida: false,
    }
  }

/*   listaAvaliados(){
    const array = this.notaEpisodio
    const lista = []
    for (let i = 0; i < array.length; i++) {
      if(array[i].nota){
        lista.push(array[i])
      }
    }
    console.log(lista)
    return lista
  }  */

/* 
  componentDidMount() {
    axios.get('https://pokeapi.co/api/v2/pokemon/10').then( response => console.log(response) )
  }
 */
  registrarNota(evt) {
  
    const { episodio } = this.state
    const nota = evt.target.value
    if (nota > 0 && nota < 6) {
      episodio.avaliar(nota)
      this.setState({
        notaValida: true,})
        this.avaliados.push(episodio)
            }
    else {
      this.setState({
        notaValida: false,})
    }
    this.setState({
      episodio,
      exibirMenssagem: true,
    })
    setTimeout(() => {
      this.setState({
        exibirMenssagem: false,
      })
    }, 3000);
   console.log(this.avaliados)
  }

  gerarCampoNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <MeuInputNumero placeHolder="max 5" msgSpan="Qual sua nota para esse episódio?" registrarNota={this.registrarNota}/>
              {/* <span>Qual sua nota para esse episódio?</span>
              <input className="inputNota" type="number" placeholder="max 5" onBlur={this.registrarNota.bind(this)}></input> */}
            </div>
          )
        }
      </div>
    )
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState(
      { episodio }
    )
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido(episodio)
    this.setState({
      episodio
    })
  }

  logout(){
    localStorage.removeItem('Authorization')
  }

  

  render() {
    const { episodio, exibirMenssagem, notaValida } = this.state
    return (
      <div className="App-blackMirror">
        <div className="header">
          <Link className="logoLink button" to='/'>Home</Link>
          <Link className="logoLink button" to='/dbcFlix'>DbcFlix</Link>
          <button type='button' className='logoLink button' onClick={this.logout.bind(this)}>Logout</button>
          <h1 className='nameHeader'>BlackMirror</h1>
        </div>
        <div className='container'>
          <EpisodioPadrao episodio={episodio} sortearNoComp={this.sortear} marcarNoComp={this.marcarComoAssistido} />
          {this.gerarCampoNota()}
          <MensagemFlash exibirMenssagem={exibirMenssagem} notaValida={notaValida} />
          <ListaDeAvaliados lista={this.avaliados}/>
          {/*  <h4>{exibirMessagem ? 'Nota registrada com sucesso!' : ''}</h4> */}
       
        </div>

      </div>
    );
  }
}

