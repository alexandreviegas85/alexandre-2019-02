//console.log("cheguei");

var teste = "123";
let teste1 = "123";
const teste2 = 1222;

var teste = "111";
//teste1 = "122";
//const teste2 = "aqui"
//console.log(teste1);

{
    let teste1 = "Aqui mudou";
    //console.log(teste1);
}
//console.log(teste1);

const pessoa = {
    nome: "Alexandre",
    idade: 33,
    endereco:{
        logradouro: "rua andarai",
        numero: 123
    }
}

function somar(valor1,valor2){
    console.log(valor1+valor2);
}

//somar(2,3);

function ondeMoro(cidade){
    //console.log("Eu moro em " +cidade + " Eu sou muito feliz");
    //console.log(`Eu moro em ${cidade} Eu sou muito feliz`);
    console.log(`Eu moro em ${cidade} Eu sou muito feliz`);
}

function fruteira(){
    let text = "Banana"
                + "\n"
                + "Banana"
                + "\n"
                + "Banana";
                console.log(text);
}

function fruteira1(){
    let newText = ` Banana
                    Banana
                    Banana`;
                console.log(newText);
}

//fruteira();
//fruteira1();


//ondeMoro("Porto Alegre");

//console.log(pessoa);

Object.freeze(pessoa);
pessoa.nome = "Joao";
//console.log(pessoa);

function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome} e tenho ${pessoa.idade}`);
}

//quemSou(pessoa);

let funcaoSomarVar = function(a,b, c=0){
    return a + b +c;
}

let add = funcaoSomarVar;
let resultado = add(3,2);
//console.log(resultado);

const{nome:n, idade:i}= pessoa
const{endereco:{logradouro, numero }}= pessoa
//console.log(n, i);
//console.log(logradouro, numero);

const array = [1,2,3,4];
const [n1, , n2, , n3=9] = array
//console.log(n1,n2,n3)

function testarPessoa({nome, idade}){
    console.log(nome, idade);
}

//testarPessoa(pessoa);

let a1 =42;
let b1 =15;

console.log(a1);
console.log(b1);

[a1,b1]=[b1,a1];

console.log(a1);
console.log(b1);

