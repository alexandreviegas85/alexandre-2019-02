
let circulo = { raio: 10, tipo: "C" }

/* function calculaCirculo({raio, tipo}){
    if(tipo == "A"){
        area = Math.PI * raio * raio;
        return  Math.ceil(area)}
    if(tipo == "C"){
        circunferencia = 2 * Math.PI * raio;
        return Math.ceil(circunferencia)}
}
console.log(calculaCirculo(circulo)); */

/* function calcularCirculo({raio, tipoCalculo:tipo}){
    return tipo == "A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio ;
}

let circulo = {
    raio:3,
    tipoCalculo:"A"
}

console.log(calcularCirculo(circulo)); */

//------------------------------------------------------------//

/* function naoBissexto(ano){
    if(ano %2 == 1){
        return true;
    }
    else{
        return false;
    }
}
let resultado1 = naoBissexto(2020);
console.log(resultado1); */

/* function naoBissexto(ano){
    return (ano % 400 == 0) || (ano %4 == 0 && ano %100 !=0) ? false : true;
    } */

let naoBissexto = ano => (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0) ? false : true;

console.log(naoBissexto(2016));

/* const array = [ 1, 2, 2, 6, 7 ];
function somarPares(array) {
    let soma=0;
    for(var i=0; i<array.length; i++) {
        if(i % 2 == 0) {
            soma = soma + array[i];
        }
    }
    return soma;
}
console.log(somarPares(array)); */

function somarPares(numeros) {
    let resultado = 0;
    for (let i = 0; i < numeros.length; i++) {
        if (i % 2 == 0) {
            resultado += numeros[i];
        }
    }
    return resultado;
}
console.log(somarPares([1, 2, 2, 2]));

/* function adicionar(op1){
    return function(op2){
        return op1+op2;
    }
} */

let adicionar = op1 => op2 => op1 + op2;

console.log(adicionar(3)(4));
console.log(adicionar(5642)(8749));


const teste = {
    diaAula: "Segunda",
    local: "DBC",
    naoBissexto(ano) {
        return (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0) ? false : true;
    }
}
//console.log(teste.naoBissexto(2016));

/* const is_divisivel = (divisor, numero) => ! (numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12)); */

const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
/* console.log(is_divisivel( 20));
console.log(is_divisivel( 11));
console.log(is_divisivel( 12)); 
 */

function arredondar(numero, precisao = 2) {
    const fator = Math.pow(10, precisao)
    return Math.ceil(numero * fator) / fator
}

function imprimirBRL(numero) {
    let qtdCasasMilhares = 3;
    let separadorMilhar = '.';
    let separadoDecimal = ',';

    let stringBuffer = []
    let parteDecimal = arredondar(Math.abs(numero) % 1)
    let parteInteira = Math.trunc(numero)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1
    while (parteInteiraString.length > 0) {
        if (c % qtdCasasMilhares == 0) {
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        } else if (parteInteiraString.length < qtdCasasMilhares) {
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)
    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    return `${parteInteira >= 0 ? 'R$' : '-R$'}${stringBuffer.reverse().join('')}${separadoDecimal}${decimalString}`
    
}
console.log(imprimirBRL(0));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-3498.99));
console.log(imprimirBRL(2313477.0135));

/************************************************************************************************/

