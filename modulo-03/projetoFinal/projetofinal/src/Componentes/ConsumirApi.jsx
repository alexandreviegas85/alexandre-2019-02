import * as axios from 'axios';

export default class ConsumirApi {
    constructor(  ) {
        this.lista = []
        this.url = 'http://localhost:1337'
    }

    requisicao( value, id ) {
        value += id ? `${id}` : ''
        axios.get(`${this.url}/${value}`, { 
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        })
        .then( resp => {
            this.lista = resp.data
            console.log(this.lista);
        }).catch(function(erro){
            console.log(erro)
        })
    }

    listaAgencias() {
        this.requisicao( 'agencias' )
    }

    listaClientes() {
        this.requisicao( 'clientes' )
    }

    listaTipoContas() {
        this.requisicao( 'tipoContas' )
    }

    listaContasClientes() {
        this.requisicao( 'conta/clientes' )
    }

    get listaTodasAgencias() {
        console.log( this.lista.agencias )
        return this.lista.agencias
    }

    get listaTodosClientes() {
        console.log( this.lista.clientes )
        return this.lista.clientes;
    }

    get listaTodosTiposContas() {
        console.log( this.lista.tipos )
        return this.lista.tipos;
    }

    get listaTodasContasClientes() {
        console.log( this.lista.cliente_x_conta )
        return this.lista.cliente_x_conta;
    }

   

}