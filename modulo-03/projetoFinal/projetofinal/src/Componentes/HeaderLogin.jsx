import React, { Component } from 'react'
import logo from '../img/logo.png'
import './Header.css'

class HeaderLogin extends Component {

    render() {
        return (
            <div className='header'>
                <img src={logo} className='logo' />
            </div>
        );
    }
}
export default HeaderLogin