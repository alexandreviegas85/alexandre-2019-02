import React, { Component } from 'react'
import logo from '../img/logo.png'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import './Header.css'

class Header extends Component {

    render() {
        return (
            <div className='header'>
                <div className='linkbotao'>
                <Link className='link' to='/'>Home</Link>
                </div>
               
                <img src={logo} className='logo' />
                
            </div>
        );
    }
}
export default Header