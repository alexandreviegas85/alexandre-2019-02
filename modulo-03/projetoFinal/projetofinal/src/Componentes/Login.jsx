import React, { Component } from 'react';
import * as axios from 'axios';
import './Login.css';
import HeaderLogin from './HeaderLogin'
import Footer from './Footer'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            senha: ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    trocaValoresState(evt) {
        const { name, value } = evt.target;
        this.setState(
            {
                [name]: value
            }
        )
    }

    logar(evt) {
        evt.preventDefault();
        const { email, senha } = this.state
        if (email && senha) {
            //executo a regra de email
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                senha: this.state.senha
            }).then(response => {
                localStorage.setItem('Authorization', response.data.token);
                this.props.history.push('/')
                //console.log(response)
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                <HeaderLogin/>
                    <div className='logar containerLogin'>
                        <h2>Login</h2>
                        <input type='text' name='email' id='nome' placeholder='login@login.com' onChange={this.trocaValoresState} />
                        <input type='password' name='senha' id='senha' placeholder='Digite sua senha' onChange={this.trocaValoresState} />
                        <button onClick={this.logar.bind(this)}>Logar</button>
                    </div>
                <Footer/>
            </React.Fragment>
        )
    }
}