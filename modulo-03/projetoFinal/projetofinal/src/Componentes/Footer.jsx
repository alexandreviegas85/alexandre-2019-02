import React from 'react'
import './Footer.css'
import twitter from '../img/twitter.png'
import facebook from '../img/facebook.png'
import instagram from '../img/instagram.png'

export default header => {
    return(
        <div className='footer'>
            <div className='par-left'>
                <p>Fale com a gente: 0800 010 1010</p>
                <p>Rua Digital, 1010 - 010101-000 - Porto Alegre, RS</p>
            </div>
            <div className='col'>
                <img src={facebook}></img>
                <img src={instagram}></img>
                <img src={twitter}></img>
            </div>
        </div>
        
    )
}