import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import PrivateRoute from './Componentes/PrivateRoute'
import Login from './Componentes/Login'
import PaginaInicial from './Pages/PaginaInicial'
import Agencias from './Pages/Agencias';
import ContaDeClientes from './Pages/ContaDeClientes';
import Clientes from './Pages/Clientes';
import TiposDeContas from './Pages/TipoDeContas';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="">
        <Router>
          <React.Fragment>
            <section className=''>
              <PrivateRoute path="/" exact component={PaginaInicial} />
              <Route path="/login" component={Login}/>
              <Route path="/agencias" component={Agencias}/>
              <Route path="/agencias/:id" component={Agencias}/>
              <Route path="/contadeclientes" component={ContaDeClientes}/>
              <Route path="/clientes" component={Clientes}/>
              <Route path="/tipodecontas" component={TiposDeContas}/>
            </section>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}
export default App

