import React, { Component } from 'react'
import ConsumirApi from '../Componentes/ConsumirApi'
import Header from '../Componentes/Header'
import Footer from '../Componentes/Footer'
import './Agencias.css'

export default class Agencias extends Component {
    constructor(props) {
        super(props)
        this.api = new ConsumirApi()
        this.state = {
            listaAgencias: null,

        }
    }

    componentWillMount() {
        this.api.listaAgencias()
        setTimeout(() => {
            this.setState({
                listaAgencias: this.api.listaTodasAgencias
            })
        }, 1000);
    }

    buscarAgPorNome(event) {
        const todasAgencias = this.api.listaTodasAgencias
        const retorno = todasAgencias.filter(e => e.nome.toLowerCase().includes(event.target.value.toLowerCase()))
        this.setState({
            listaAgencias: retorno
        })
    }
    buscarAgPorCodigo(event) {
        const todasAgencias = this.api.listaTodasAgencias
        const retorno = todasAgencias.filter(e => String(e.codigo) === event.target.value)
        this.setState({
            listaAgencias: retorno
        })
    }

    buscarIsDigital() {
        const todasAgencias = this.api.listaTodasAgencias
        const retorno = todasAgencias.filter(e => e.isDigital === true)
        this.setState({
            listaAgencias: retorno
        })
    }

    marcarComoDigital(event) {
        const { listaAgencias } = this.state
        const ag = listaAgencias.find(e => e.id === event)
        ag.isDigital = !ag.isDigital
        console.log(listaAgencias)
        this.setState({
            listaAgencias
        })
    }

    render() {
        const { listaAgencias } = this.state
        return (
            <React.Fragment>
                <div>
                    {listaAgencias ? (<div >
                        <Header />
                        <div>
                            <button className='buttonIsDigital' onClick={this.buscarIsDigital.bind(this)}>Filtrar IsDigital</button>
                            <input className='input' type='text' placeholder='filtrar por nome agência' onChange={this.buscarAgPorNome.bind(this)}></input>
                            <input className='input' type='text' placeholder='filtrar por código da agência' onChange={this.buscarAgPorCodigo.bind(this)}></input>
                        </div>
                        <div className='ag'>
                            {listaAgencias.map(e => {
                                return (
                                    <React.Fragment>
                                        <div className='cardAg'>
                                            <p>Código Ag: {e.codigo}</p>
                                            <p>Nome: {e.nome}</p>
                                            <p>Logradouro: {e.endereco.logradouro}</p>
                                            <p>Numero: {e.endereco.numero}</p>
                                            <p>Bairro: {e.endereco.bairro}</p>
                                            <p>Cidade: {e.endereco.cidade}</p>
                                            <p>UF: {e.endereco.uf}</p>
                                            <button className='buttonIsDigital' onClick={() => this.marcarComoDigital(e.id)}>IsDigital</button>
                                        </div>
                                    </React.Fragment>
                                )
                            })}
                        </div>
                    </div>
                    ) : <h2>Carregando!</h2>}
                    <Footer />
                </div>
            </React.Fragment>
        )
    }
}