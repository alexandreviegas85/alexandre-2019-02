import React, { Component } from 'react'
import ConsumirApi from '../Componentes/ConsumirApi'
import Header from '../Componentes/Header'
import Footer from '../Componentes/Footer'


export default class Clientes extends Component {
    constructor(props) {
        super(props)
        this.api = new ConsumirApi()
        this.state = {
            listaClientes: null
        }
    }

    componentWillMount() {
        this.api.listaClientes()
        setTimeout(() => {
            this.setState({
                listaClientes: this.api.listaTodosClientes
            })
        }, 1000);
    }

    buscarCliPorNome(event) {
        const todasClientes = this.api.listaTodosClientes
        const retorno = todasClientes.filter(e => e.nome.toLowerCase().includes(event.target.value.toLowerCase()))
        this.setState({
            listaClientes: retorno
        })
    }

    render() {
        const { listaClientes } = this.state
        return (
            <React.Fragment>
                <div>
                    {listaClientes ? (<div >
                        <Header />
                        <div>
                            <input className='input' type='text' placeholder='filtrar por nome do cliente' onChange={this.buscarCliPorNome.bind(this)}></input>
                        </div>
                        <div className='ag'>
                            {listaClientes.map(cli => {
                                return (
                                    <React.Fragment>
                                        <div className='cardAg'>
                                            <p>Nome: {cli.nome}</p>
                                            <p>Cpf: {cli.cpf}</p>
                                        </div>
                                    </React.Fragment>
                                )
                            })}
                        </div>
                    </div>
                    ) : <h2>Carregando!</h2>}
                    <Footer />
                </div>
            </React.Fragment>
        )
    }
}