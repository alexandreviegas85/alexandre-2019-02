import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import HeaderLogin from '../Componentes/HeaderLogin'
import Footer from '../Componentes/Footer'
import './PaginaInicial.css'

export default class PaginaInicial extends Component {

    logout() {
        localStorage.removeItem('Authorization')
    }

    render() {
        return (
            <React.Fragment>
                <HeaderLogin />
                <button type='button' className='buttonLogout' onClick={this.logout.bind(this)}>
                    <Link className='b1' to='/'>Logout</Link></button>
                <div className='pg container'>
                    <div className='card'>
                        <Link className='textCard' to='/agencias'>Agências</Link>
                    </div>
                    <div className='card'>
                        <Link className="textCard" to='/clientes'>Clientes</Link>
                    </div>
                    <div className='card'>
                        <Link className="textCard" to='/contadeclientes'>Conta De Clientes</Link>
                    </div>
                    <div className='card'>
                        <Link className="textCard" to='/tipodecontas'>Tipo de Contas</Link>
                    </div>
                </div>
                <Footer />
            </React.Fragment>
        )
    }
}