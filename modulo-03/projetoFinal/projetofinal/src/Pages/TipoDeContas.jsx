import React, { Component } from 'react'
import ConsumirApi from '../Componentes/ConsumirApi'
import Header from '../Componentes/Header'
import Footer from '../Componentes/Footer'

export default class TipoDeContas extends Component {
    constructor(props) {
        super(props)
        this.api = new ConsumirApi()
        this.state = {
            listaTipoContas: null
        }
    }

    componentWillMount() {
        this.api.listaTipoContas()
        setTimeout(() => {
            this.setState({
                listaTipoContas: this.api.listaTodosTiposContas
            })
        }, 1000);
    }

    buscarTipoDeConta( event ) {
        const todasTipos = this.api.listaTodosTiposContas
        const retorno = todasTipos.filter(e => e.nome.toLowerCase().includes(event.target.value.toLowerCase()))
        this.setState({
            listaTipoContas: retorno
        })
    }

    render() {
        const { listaTipoContas } = this.state
        return (
            <React.Fragment>
                <div>
                    {listaTipoContas ? (<div >
                        <Header />
                        <div>
                            <input className='input' type='text' placeholder='filtrar por tipo de conta' onChange={this.buscarTipoDeConta.bind(this)}></input>
                        </div>
                        <div className='ag'>
                            {listaTipoContas.map(tc => {
                                return (
                                    <React.Fragment>
                                        <div className='cardAg'>
                                            <p>Id: {tc.id}</p>
                                            <p>Tipo: {tc.nome}</p>
                                        </div>
                                    </React.Fragment>
                                )
                            })}
                        </div>
                    </div>
                    ) : <h2>Carregando!</h2>}
                    <Footer />
                </div>
            </React.Fragment>
        )
    }
}