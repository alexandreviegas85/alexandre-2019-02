import React, { Component } from 'react'
import ConsumirApi from '../Componentes/ConsumirApi'
import Header from '../Componentes/Header'
import Footer from '../Componentes/Footer'


export default class ContaDeClientes extends Component {
    constructor(props) {
        super(props)
        this.api = new ConsumirApi()
        this.state = {
            listaContasClientes: null
        }
    }

    componentWillMount() {
        this.api.listaContasClientes()
        setTimeout(() => {
            this.setState({
                listaContasClientes: this.api.listaTodasContasClientes
            })
        }, 1000);
    }

    buscarCliPorNome( event ) {
        const todosClientes = this.api.listaTodasContasClientes
        /* console.log(todosClientes)
        console.log(event.target.value.toLowerCase()) */
        const retorno = todosClientes.filter(e => e.cliente.nome.toLowerCase().includes(event.target.value.toLowerCase()))
        this.setState({
            listaContasClientes: retorno
        })
    }

    buscarCliPorTipoConta( event ) {
        const todosClientes = this.api.listaTodasContasClientes
        /* console.log(todosClientes)
        console.log(event.target.value.toLowerCase()) */
        const retorno = todosClientes.filter(e => e.tipo.nome.toLowerCase().includes(event.target.value.toLowerCase()))
        this.setState({
            listaContasClientes: retorno
        })
    }


    render() {
        const { listaContasClientes } = this.state
        return (
            <React.Fragment>
                <div>
                    {listaContasClientes ? (<div >
                        <Header />
                        <div>
                            <input className='input' type='text' placeholder='filtrar por nome do cliente' onChange={this.buscarCliPorNome.bind(this)}></input>
                            <input className='input' type='text' placeholder='filtrar por tipo de conta' onChange={this.buscarCliPorTipoConta.bind(this)}></input>
                        </div>
                        <div className='ag'>
                            {listaContasClientes.map(cli => {
                                return (
                                    <React.Fragment>
                                        <div className='cardAg'>
                                            <p>Código Ag: {cli.codigo}</p>
                                            <p>Tipo de conta: {cli.tipo.nome}</p>
                                            <p>Nome: {cli.cliente.nome}</p>
                                            <p>Cpf: {cli.cliente.cpf}</p>
                                        </div>
                                    </React.Fragment>
                                )
                            })}
                        </div>
                    </div>
                    ) : <h2>Carregando!</h2>}
                    <Footer />
                </div>
            </React.Fragment>
        )
    }
}