/* eslint-disable no-unused-vars */
class Pokemon {
  constructor( obj ) {
    this.imagem = obj.sprites.front_default
    this.nome = obj.name
    this.idPoke = obj.id
    this.peso = obj.weight
    this.altura = obj.height
    this.tipo = obj.types.map( item => item.type.name ).toString()
    this.estatisticas = obj.stats.map( item => ` ${ item.stat.name } ${ item.base_stat }%` ).toString()
  }

  conversaoAltura( multiplicador = 10 ) {
    return this.altura * multiplicador
  }

  conversaoPeso( divisor = 10 ) {
    return this.peso / divisor
  }
}
