/* eslint-disable no-unused-vars */
class PokeApi {
  buscarTodos() {
    const pokemon = fetch( 'https://pokeapi.co/api/v2/pokemon/' )
    return pokemon.then( data => data.json() )
  }

  buscarEspecifico( id ) {
    const pokemon = fetch( `https://pokeapi.co/api/v2/pokemon/${ id }` )
    return pokemon.then( data => data.json() )
  }
}
