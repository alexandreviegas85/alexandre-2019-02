
/* eslint-disable no-use-before-define */
let pokemonAtual = { id: 0 }
const pokeApi = new PokeApi()
const id = document.getElementById( 'idNumero' )
id.addEventListener( 'blur', () => {
  if ( !id.value || pokemonAtual.id === id.value ) {
    return
  }
  const pokemonEspecifico = pokeApi.buscarEspecifico( id.value )
  pokemonEspecifico.then( pokemon => {
    const poke = new Pokemon( pokemon )
    renderizacaoPokemon( poke )
  } )
    .catch( () => {
      /* document.getElementById( 'erro' ).innerHTML = 'Digite um id válido!' */
      console.log( 'Digite um id válido!' )
    } )
} )

/* async function buscar() {
  let pokemonEspecifico = await pokeApi.buscarEspecifico( id.value );
  let poke = new Pokemon( pokemonEspecifico )
  renderizaPokemon( poke );
}
buscar(); */

function renderizacaoPokemon( pokemon ) {
  pokemonAtual = pokemon;
  const dadosPokemon = document.getElementById( 'dodosPokemon' )

  dadosPokemon.querySelector( '.nome' ).innerHTML = `Nome: ${ pokemon.nome }`
  dadosPokemon.querySelector( '.imagem' ).innerHTML = `<img src="${ pokemon.imagem }"></img>`
  dadosPokemon.querySelector( '.idPoke' ).innerHTML = `Id: ${ pokemon.idPoke }`
  dadosPokemon.querySelector( '.peso' ).innerHTML = `Peso: ${ pokemon.conversaoPeso() } Kg`
  dadosPokemon.querySelector( '.altura' ).innerHTML = `Altura: ${ pokemon.conversaoAltura() } Cm`
  dadosPokemon.querySelector( '.tipo' ).innerHTML = `Tipo: ${ pokemon.tipo }`
  dadosPokemon.querySelector( '.estatistica' ).innerHTML = `Estatísticas: ${ pokemon.estatisticas }`
}

/* Exercício 03-Estou com sorte!
Add botão “Estou com sorte”,que quando clicado, exibe um pokémon aleatório na tela(entre 1 e 802) */

const sorte = document.getElementById( 'sorte' )
sorte.addEventListener( 'click', () => {
  const numero = Math.floor( Math.random() * 802 + 1 );
  const pokemonAleatorio = pokeApi.buscarEspecifico( numero );
  pokemonAleatorio.then( pokemon => {
    const poke = new Pokemon( pokemon );
    renderizacaoPokemon( poke );
  } )
} )
