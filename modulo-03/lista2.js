

function imprimirGBP(numero) {
    let qtdCasasMilhares = 3;
    let separadorMilhar = ',';
    let separadoDecimal = '.';

    let stringBuffer = []
    let parteDecimal = arredondar(Math.abs(numero) % 1)
    let parteInteira = Math.trunc(numero)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1
    while (parteInteiraString.length > 0) {
        if (c % qtdCasasMilhares == 0) {
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        } else if (parteInteiraString.length < qtdCasasMilhares) {
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)
    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    return `${parteInteira >= 0 ? '£' : '-£'}${stringBuffer.reverse().join('')}${separadoDecimal}${decimalString}`
}
/* console.log(imprimirGBP(0));
console.log(imprimirGBP(3498.99));
console.log(imprimirGBP(-3498.99));
console.log(imprimirGBP(2313477.0135)); */

/************************************************************************************************/

function imprimirFR(numero) {
    let qtdCasasMilhares = 3;
    let separadorMilhar = '.';
    let separadoDecimal = ',';

    let stringBuffer = []
    let parteDecimal = arredondar(Math.abs(numero) % 1)
    let parteInteira = Math.trunc(numero)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1
    while (parteInteiraString.length > 0) {
        if (c % qtdCasasMilhares == 0) {
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        } else if (parteInteiraString.length < qtdCasasMilhares) {
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)
    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    return `${parteInteira >= 0 ? '' : '-'}${stringBuffer.reverse().join('')}${separadoDecimal}${decimalString} €`
}
/* console.log(imprimirFR(0));
console.log(imprimirFR(3498.99));
console.log(imprimirFR(-3498.99));
console.log(imprimirFR(2313477.0135)); */


/************************************************************************************************************************************/
let moeda = (function () {
    function imprimirMoeda({ numero, separadorMilhar, separadorDecimal, colocarMoeda, colocarNegativo }) {
        function arredondar(numero, precisao = 2) {
            const fator = Math.pow(10, precisao);
            return Math.ceil(numero * fator) / fator;
        }

        let qtdCasaMilhares = 3;

        let stringBuffer = [];
        let parteDecimal = arredondar(Math.abs(numero) % 1);
        let parteInteira = Math.trunc(numero);
        let parteInteiraString = Math.abs(parteInteira).toString();
        let parteInteiraTamanho = parteInteiraString.length;

        let c = 1;
        while (parteInteiraString.length > 0) {
            if (c % qtdCasaMilhares == 0) {
                stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
                parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c);
            }
            else if (parteInteiraString.length < qtdCasaMilhares) {
                stringBuffer.push(parteInteiraString);
                parteInteiraString = '';
            }
            c++;
        }
        stringBuffer.push(parteInteiraString);

        let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0');
        const numeroFormatado = `${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`;
        return parteInteira > 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado));
    }
    return {
        imprimirGBP: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar: ',',
                separadorDecimal: '.',
                colocarMoeda: numeroFormatado => `£ ${numeroFormatado}`,
                colocarNegativo: numeroFormatado => `-${numeroFormatado}`,
            }),
        imprimirBRL: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `R$ ${numeroFormatado}`,
                colocarNegativo: numeroFormatado => `-${numeroFormatado}`,
            }),
        imprimirFR: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar: ',',
                separadorDecimal: '.',
                colocarMoeda: numeroFormatado => `${numeroFormatado} €`,
                colocarNegativo: numeroFormatado => `-${numeroFormatado}`,
            })
    }
})();

        
console.log(moeda.imprimirGBP(0));
console.log(moeda.imprimirGBP(3498.99));
console.log(moeda.imprimirGBP(-3498.99));
console.log(moeda.imprimirGBP(2313477.0135));

console.log(moeda.imprimirFR(0));
console.log(moeda.imprimirFR(3498.99));
console.log(moeda.imprimirFR(-3498.99));
console.log(moeda.imprimirFR(2313477.0135));

console.log(moeda.imprimirBRL(0));
console.log(moeda.imprimirBRL(3498.99));
console.log(moeda.imprimirBRL(-3498.99));
console.log(moeda.imprimirBRL(2313477.0135));