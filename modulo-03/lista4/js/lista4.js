//Criar uma função que receba um multiplicador e vários valores a serem multiplicados, o retorno deve ser um array!
function multiplicar(multiplicador, ...valores) {
    return valores.map(valor => valor * multiplicador)
}
//console.log(multiplicar(5, 3, 4))
//console.log(multiplicar(5, 3, 4, 5))

//Criar um formulário de contato no site de vocês (Nome, email, telefone, assunto), 
//ao sair do input de nome tem que garantir que tenha no minimo 10 caracteres,
//garantir que em email tenha @ e ao clicar no botão de enviar tem que garantir que não tenha campos em branco.



function nomeMin10() {
    let nome = document.getElementById("nome")
    if (nome.value.length < 10) {
        alert('informar nome com no minimo 10 caracteres');
    }
}

function e_mail() {
    let email = document.getElementById("email")
    if (!email.value.includes('@')) {
        alert("Preencha campo E-MAIL corretamente!")
    }
}

function validaCampoAssunto() {
    let assunto = document.getElementById("assunto")
    if (!assunto.value.length > 0) {
        alert("Preencha o campo Assunto!")
    }
}

function validaCampoMensagem() {
    let mensagem = document.getElementById('mensagem')
    if (!mensagem.value.length > 0) {
        alert("Preencha o campo Mensagem!")
    }
} 

function validarCamposSemValor() {
    if( !(
        document.getElementById("nome").value.length > 0 && 
        document.getElementById("email").value.length > 0 &&
        document.getElementById("assunto").value.length > 0 &&
        document.getElementById('mensagem').value.length > 0 
        )
    ){
        alert("Existem campos não preenchidos!");
    }

}
