import React from 'react';

const EpisodioPadrao = props => {
    const { episodio } = props
    return (
        <React.Fragment>
            
            <h1>{episodio.nome}</h1>
            <img src={episodio.thumbUrl} alt={episodio.nome}></img>
            <h3>Já assistido? {episodio.assistido ? 'Sim' : 'Não'}, {episodio.qtdVezesAssistido} vez(es)</h3>
            <p>Temp/Ep: {episodio.temporadaEpisodio}</p>
            <p>Duração: {episodio.duracaoEmMin} </p>
            <h4>{episodio.nota || 'sem nota'}</h4>
            <div>
                <button onClick={ props.sortearNoComp }>Próximo</button>
                <button onClick={ props.marcarNoComp }>Já Assisti</button>
            </div>
        </React.Fragment>
    )
}
export default EpisodioPadrao