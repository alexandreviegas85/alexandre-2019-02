
import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/ListaEpisodios'
//import EpisodioPadrao from './Components/EpisodioPadrao'
import TesteRenderizacao from './Components/TesteRenderizacao'

class App extends Component {
  constructor(props) {
    super(props)
    this.listaEpisodios = new ListaEpisodios()
    this.sortear = this.sortear.bind(this)
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMessagem: false
    }
  }

  registrarNota(evt) {
    const { episodio } = this.state
    episodio.avaliar(evt.target.value)
    this.setState({
      episodio,
      exibirMessagem: true
    })
    setTimeout(() => {
      this.setState({
        exibirMessagem: false
      })
    }, 3000);
  }

  gerarCampoNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para esse episódio?</span>
              <input className="inputNota" type="number" placeholder="max 5" onBlur={this.registrarNota.bind(this)}></input>
            </div>
          )
        }
      </div>
    )
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState(
      { episodio }
    )
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido(episodio)
    this.setState({
      episodio
    })
  }

  render() {
    const { episodio, exibirMessagem } = this.state
    return (
      <div className="App">
        <div className='App-Header'>
          <EpisodioPadrao episodio={episodio} sortearNoComp={this.sortear} marcarNoComp={this.marcarComoAssistido} />
          {this.gerarCampoNota()}
          <h4>{exibirMessagem ? 'Nota registrada com sucesso!' : ''}</h4>
         {/*  <TesteRenderizacao nome={"marcos"} >
            <h4>teste</h4>
          </TesteRenderizacao> */}
        </div>
      </div>
    );
  }
}

export default App;
