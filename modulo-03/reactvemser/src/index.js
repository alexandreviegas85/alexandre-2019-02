import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

/* const elemento = 
(<div className="classTest">
<ul>
    <li>Porto Alegre</li>
    <li>Esteio</li>
    <li>Canoas</li>
</ul>
</div>) */

ReactDOM.render(<App/>, document.getElementById('root'));

serviceWorker.unregister();
