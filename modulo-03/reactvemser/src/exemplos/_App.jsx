import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import ListaTimes from '../models/ListaEpisodios'
//import CompA, { CompB } from './ExemploComponenteBasico';
//import Filho from './exemplos/Filhos'
import Familia from './Familia'


class App extends Component {
  constructor( props ) {
    super (props )
    this.listaTimes = new ListaTimes()
    console.log(this.listaTimes)
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <Filho titulo = { 'Nome'}/> */}
          <Familia nome = {'Antonio'} sobrenome={'Silva'}></Familia>
          <Familia nome = {'Joao'} sobrenome={'Pereira'}></Familia>
          <Familia nome = {'Maria'} sobrenome={'Silva'}></Familia>
          <Familia nome = {'Fabio'} sobrenome={'Pereira'}></Familia>

          <ul>
            {this.listaTimes.todos.map((time) => {
              return `Time: ${time.nome} Estado: ${time.estado} | ` 
            } )}
          </ul>
        </header>
      </div>
    );
  }

}

export default App;
