package br.com.dbccompany.coworking.Repository;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.dbccompany.coworking.Entity.Usuarios;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuariosRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Test
    public void procurarUsuarioPorNome(){
        Usuarios usuarios = new Usuarios();
        usuarios.setNome("Joao");
        entityManager.persist(usuarios);
        entityManager.flush();

        Usuarios resposta = usuariosRepository.findByNome(usuarios.getNome());

        assertThat(resposta.getNome()).isEqualTo(usuarios.getNome());
    }
}
