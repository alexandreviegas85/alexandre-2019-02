package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.TipoContato;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TipoContatoRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void procurarTipoContatoPorNome(){
        TipoContato tipoContato = new TipoContato();
        tipoContato.setNome("Email");
        entityManager.persist(tipoContato);
        entityManager.flush();

        TipoContato resposta = tipoContatoRepository.findByNome(tipoContato.getNome());

        assertThat(resposta.getNome()).isEqualTo(tipoContato.getNome());

    }
}
