package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;

public interface EspacosRepository extends CrudRepository<Espacos, Integer> {
}
