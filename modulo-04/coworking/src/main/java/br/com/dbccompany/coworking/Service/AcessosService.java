package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.SaldoClienteID;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository acessosRepository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public Acessos salvar(Acessos acessos){


        return acessosRepository.save(acessos);
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos editar(Integer id, Acessos acessos){
        acessos.setId(id);
        return acessosRepository.save(acessos);
    }

    public List<Acessos> todosAcessos(){
        return (List<Acessos>) acessosRepository.findAll();
    }

}
