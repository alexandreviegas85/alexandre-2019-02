package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacosPacotes")
public class EspacosPacotesController {

    @Autowired
    EspacosPacotesService espacosPacotesService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<EspacosPacotes> todosEspacosPacotes(){
        return espacosPacotesService.todosEspacosPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  EspacosPacotes novoEspacosPacotes(@RequestBody EspacosPacotes x){
        return  espacosPacotesService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public EspacosPacotes editarEspacosPacotes(@PathVariable Integer id, @RequestBody EspacosPacotes x){
        return  espacosPacotesService.editar(id, x);
    }
}
