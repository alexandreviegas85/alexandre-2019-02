package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteID;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente){
        return saldoClienteRepository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editar(SaldoClienteID id, SaldoCliente saldoCliente){
        saldoCliente.setId(id);
        return saldoClienteRepository.save(saldoCliente);
    }

    public List<SaldoCliente> todosSaldoCliente(){
        return (List<SaldoCliente>) saldoClienteRepository.findAll();
    }
}
