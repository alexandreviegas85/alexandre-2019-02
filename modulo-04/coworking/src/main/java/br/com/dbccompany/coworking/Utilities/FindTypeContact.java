package br.com.dbccompany.coworking.Utilities;

import br.com.dbccompany.coworking.Entity.Contato;

import java.util.List;

public class FindTypeContact {

    public boolean findTypeEmailOrPhone(List<Contato> lista){
        for (Contato c : lista) {
            if (c.getTipoContato().getNome().equalsIgnoreCase("Email")) {
                for (Contato c2 : lista){
                    if (c2.getTipoContato().getNome().equalsIgnoreCase("Telefone")){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
