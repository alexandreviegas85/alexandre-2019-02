package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacotes")
public class PacotesController {

    @Autowired
    PacotesService pacotesService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pacotes> todosPacotes(){
        return pacotesService.todosPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  Pacotes novoPacotes(@RequestBody Pacotes x){
        return  pacotesService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public Pacotes editarPacotes(@PathVariable Integer id, @RequestBody Pacotes x){
        return  pacotesService.editar(id, x);
    }
}
