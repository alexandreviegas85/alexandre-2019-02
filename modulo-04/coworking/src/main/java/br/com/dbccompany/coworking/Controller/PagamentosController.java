package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    @Autowired
    PagamentosService pagamentosService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pagamentos> todosPagamentos(){
        return pagamentosService.todosPagamentos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  Pagamentos novoPagamentos(@RequestBody Pagamentos x){
        return  pagamentosService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public Pagamentos editarPagamentos(@PathVariable Integer id, @RequestBody Pagamentos x){
        return  pagamentosService.editar(id, x);
    }
}
