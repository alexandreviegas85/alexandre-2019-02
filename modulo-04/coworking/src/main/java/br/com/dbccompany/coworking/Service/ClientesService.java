package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Utilities.FindTypeContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository clientesRepository;

    @Transactional(rollbackFor = Exception.class)
    public Clientes salvar(Clientes clientes){
        FindTypeContact contact = new FindTypeContact();
        boolean found = contact.findTypeEmailOrPhone(clientes.getContatos());
        if(found){
            return clientesRepository.save(clientes);
        }
        System.out.println("No valid contact type found.");
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Clientes editar(Integer id, Clientes clientes){
        clientes.setId(id);
        return clientesRepository.save(clientes);
    }

    public Clientes getById(Integer id){
        return clientesRepository.findById(id).get();
    }

    public List<Clientes> todosClientes(){
        return (List<Clientes>) clientesRepository.findAll();
    }

}
