package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CLIENTES_PACOTES")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Acessos.class)
@SequenceGenerator(allocationSize = 1, name = "CLIENTE_PACOTE_SEQ", sequenceName = "CLIENTE_PACOTE_SEQ")
public class ClientesPacotes extends AbstractEntity {

    @Id
    @Column(name = "ID_CLIENTE_PACOTE")
    @GeneratedValue(generator = "CLIENTE_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "FK_ID_CLIENTE")
    private Clientes idClientes;

    @ManyToOne
    @JoinColumn(name = "FK_ID_PACOTE")
    private Pacotes pacotes;

    @OneToMany(mappedBy = "clientesPacotes")
    List<Pagamentos> pagamentos = new ArrayList<>();

    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    //getters and setters

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(Clientes idClientes) {
        this.idClientes = idClientes;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
