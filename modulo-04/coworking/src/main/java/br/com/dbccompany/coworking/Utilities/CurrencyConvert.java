package br.com.dbccompany.coworking.Utilities;

import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyConvert {

//    public static String converteMoeda(Double moedaFormat){
//        NumberFormat numberFormat= new java.text.DecimalFormat("R$: ##,#0.00");//seu formatador
//        String formatado = numberFormat.format(moedaFormat);
//        return formatado;
//    }

    public static String currencyToReais(double valor){
        NumberFormat number = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        return number.format(valor);
    }

    public static Double toDouble(String valor){
        return Double.parseDouble(valor);
    }

}
