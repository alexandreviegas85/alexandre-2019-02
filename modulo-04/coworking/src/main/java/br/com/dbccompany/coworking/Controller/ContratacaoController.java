package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService contratacaoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contratacao> todosContratacao(){
        return contratacaoService.todosContratacao();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public String novoContratacao(@RequestBody Contratacao x){
        return  contratacaoService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public Contratacao editarContratacao(@PathVariable Integer id, @RequestBody Contratacao x){
        return  contratacaoService.editar(id, x);
    }
}
