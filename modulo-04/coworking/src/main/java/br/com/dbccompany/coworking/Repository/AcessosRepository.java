package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Acessos;
import org.springframework.data.repository.CrudRepository;

public interface AcessosRepository extends CrudRepository<Acessos, Integer> {
}
