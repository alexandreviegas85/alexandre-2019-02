package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    @Autowired
    TipoContatoService tipoContatoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<TipoContato> todosTipoContato(){
        return tipoContatoService.todosTipoContato();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  TipoContato novoTipoContato(@RequestBody TipoContato x){
        return  tipoContatoService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public TipoContato editarTipoContato(@PathVariable Integer id, @RequestBody TipoContato x){
        return  tipoContatoService.editar(id, x);
    }
}
