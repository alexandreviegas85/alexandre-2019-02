package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Repository.ClientesPacotesRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository pagamentosRepository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private ClientesPacotesRepository clientesPacotesRepository;

    @Transactional(rollbackFor = Exception.class)
    public Pagamentos salvar(Pagamentos pagamentos){
        Contratacao contratacao = contratacaoRepository.findById(pagamentos.getContratacao().getId()).get();
        pagamentos.setContratacao(contratacao);

        ClientesPacotes clientesPacotes = clientesPacotesRepository.findById(pagamentos.getClientesPacotes().getId()).get();
        pagamentos.setClientesPacotes(clientesPacotes);

        return pagamentosRepository.save(pagamentos);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamentos editar(Integer id, Pagamentos pagamentos){
        pagamentos.setId(id);
        return pagamentosRepository.save(pagamentos);
    }

    public List<Pagamentos> todosPagamentos(){
        return (List<Pagamentos>) pagamentosRepository.findAll();
    }
}
