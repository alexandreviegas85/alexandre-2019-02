package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {

    @Autowired
    EspacosService espacosService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espacos> todosEspacos(){
        return espacosService.todosEspacos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  Espacos novoEspacos(@RequestBody Espacos x){
        return  espacosService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public Espacos editarEspacos(@PathVariable Integer id, @RequestBody Espacos x){
        return  espacosService.editar(id, x);
    }
}
