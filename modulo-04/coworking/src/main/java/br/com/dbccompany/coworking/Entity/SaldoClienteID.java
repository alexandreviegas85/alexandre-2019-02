package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SaldoClienteID implements Serializable {

    @Column(name = "ID_ESPACO")
    private Integer idEspaco;

    @Column(name = "ID_CLIENTE")
    private  Integer idCliente;

    public SaldoClienteID(Integer idEspaco, Integer idCliente) {
        this.idEspaco = idEspaco;
        this.idCliente = idCliente;
    }

    public SaldoClienteID() {
    }

    //getters and setters

    public Integer getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco(Integer idEspaco) {
        this.idEspaco = idEspaco;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
}
