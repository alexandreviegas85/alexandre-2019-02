package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import org.springframework.data.repository.CrudRepository;

public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotes, Integer> {
}
