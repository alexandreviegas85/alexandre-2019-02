package br.com.dbccompany.coworking.Utilities;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptionMD5 {

    public String md5(String password){
        if(password == null || password.length() < 6) return null;
        String passwordMd5 = null;
        try{
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(password.getBytes(), 0, password.length());

            passwordMd5 = new BigInteger(1, digest.digest()).toString(16);
        }catch (NoSuchAlgorithmException e){
            System.err.println("Error in encrypting.");
            return null;
        }
     return passwordMd5;
    }
}
