package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import br.com.dbccompany.coworking.Utilities.EncryptionMD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Transactional(rollbackFor = Exception.class)
    public Usuarios salvar(Usuarios usuarios) {
        String senha = usuarios.getSenha();
        EncryptionMD5 md5 = new EncryptionMD5();
        senha = md5.md5(senha);
        if (senha != null){
            usuarios.setSenha(senha);
            return usuariosRepository.save(usuarios);
        }else {
            System.out.println("Error encrypting password");
            return null;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuarios editar(Integer id, Usuarios usuarios){
        usuarios.setId(id);
        return usuariosRepository.save(usuarios);
    }

    public Usuarios getById(Integer id){
        return usuariosRepository.findById(id).get();
    }

    public List<Usuarios> todosUsuarios(){
        return (List<Usuarios>) usuariosRepository.findAll();
    }
}
