package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    @Autowired
    AcessosService acessosService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Acessos> todosAcessos(){
        return acessosService.todosAcessos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  Acessos novoAcesso(@RequestBody Acessos acessos){
        return  acessosService.salvar(acessos);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public Acessos editarAcessos(@PathVariable Integer id, @RequestBody Acessos acessos){
        return  acessosService.editar(id, acessos);
    }
}
