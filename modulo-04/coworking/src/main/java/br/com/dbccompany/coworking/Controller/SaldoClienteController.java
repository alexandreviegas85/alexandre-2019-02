package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteID;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/saldoCliente")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService saldoClienteService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<SaldoCliente> todosSaldoCliente(){
        return saldoClienteService.todosSaldoCliente();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  SaldoCliente novoSaldoCliente(@RequestBody SaldoCliente x){
        return  saldoClienteService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public SaldoCliente editarSaldoCliente(@PathVariable SaldoClienteID id, @RequestBody SaldoCliente x){
        return  saldoClienteService.editar(id, x);
    }
}
