package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;

public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotes, Integer> {
}
