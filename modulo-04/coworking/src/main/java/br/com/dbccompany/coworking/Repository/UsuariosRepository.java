package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Usuarios;
import org.springframework.data.repository.CrudRepository;

public interface UsuariosRepository extends CrudRepository<Usuarios, Integer> {

    Usuarios findByNome (String nome);
}
