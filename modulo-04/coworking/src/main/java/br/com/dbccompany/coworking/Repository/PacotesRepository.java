package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;

public interface PacotesRepository extends CrudRepository<Pacotes, Integer> {
}
