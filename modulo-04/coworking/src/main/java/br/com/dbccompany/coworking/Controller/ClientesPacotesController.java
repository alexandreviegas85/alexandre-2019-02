package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientesPacotesController {

    @Autowired
    ClientesPacotesService clientesPacotesService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<ClientesPacotes> todosClientesPacotes(){
        return clientesPacotesService.todosClientesPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  ClientesPacotes novoClientesPacotes(@RequestBody ClientesPacotes x){
        return  clientesPacotesService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public ClientesPacotes editarClientesPacotes(@PathVariable Integer id, @RequestBody ClientesPacotes x){
        return  clientesPacotesService.editar(id, x);
    }
}
