package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController {

    @Autowired
    ClientesService clientesService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Clientes> todosClientes(){
        return clientesService.todosClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  Clientes novoClientes(@RequestBody Clientes x){
        return  clientesService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public Clientes editarClientes(@PathVariable Integer id, @RequestBody Clientes x){
        return  clientesService.editar(id, x);
    }
}
