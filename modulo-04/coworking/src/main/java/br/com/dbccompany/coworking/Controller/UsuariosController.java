package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/usuarios")
public class UsuariosController {

    @Autowired
    UsuarioService usuarioService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Usuarios> todosUsuarios(){
        return usuarioService.todosUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  Usuarios novoUsuarios(@RequestBody Usuarios x) {
        return  usuarioService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public Usuarios editarUsuarios(@PathVariable Integer id, @RequestBody Usuarios x){
        return  usuarioService.editar(id, x);
    }
}
