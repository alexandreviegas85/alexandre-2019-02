package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Transactional(rollbackFor = Exception.class)
    public TipoContato salvar(TipoContato tipoContato){
        return tipoContatoRepository.save(tipoContato);
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContato editar(Integer id, TipoContato tipoContato){
        tipoContato.setId(id);
        return tipoContatoRepository.save(tipoContato);
    }

    public TipoContato getById(Integer id){
       return tipoContatoRepository.findById(id).get();
    }

    public List<TipoContato> todosTipoContato(){
        return (List<TipoContato>) tipoContatoRepository.findAll();
    }
}
