package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

    @Autowired
    ContatoService contatoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contato> todosContato(){
        return contatoService.todosContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public  Contato novoContato(@RequestBody Contato x){
        return  contatoService.salvar(x);
    }

    @PutMapping(value = "editar/{id}")
    @ResponseBody
    public Contato editarContato(@PathVariable Integer id, @RequestBody Contato x){
        return  contatoService.editar(id, x);
    }
}
