package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Repository.EspacosPacotesRepository;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacosPacotesService {

    @Autowired
    private EspacosPacotesRepository espacosPacotesRepository;

    @Autowired
    private PacotesRepository pacotesRepository;

    @Autowired
    private EspacosRepository espacosRepository;

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes salvar(EspacosPacotes espacosPacotes){
        Pacotes pacotes = pacotesRepository.findById(espacosPacotes.getPacotes().getId()).get();
        espacosPacotes.setPacotes(pacotes);

        Espacos espacos = espacosRepository.findById(espacosPacotes.getEspacos().getId()).get();
        espacosPacotes.setEspacos(espacos);

        return espacosPacotesRepository.save(espacosPacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes editar(Integer id, EspacosPacotes espacosPacotes){
        espacosPacotes.setId(id);
        return espacosPacotesRepository.save(espacosPacotes);
    }

    public List<EspacosPacotes> todosEspacosPacotes(){
        return (List<EspacosPacotes>) espacosPacotesRepository.findAll();
    }
}
