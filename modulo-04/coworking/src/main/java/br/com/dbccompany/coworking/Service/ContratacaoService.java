package br.com.dbccompany.coworking.Service;


import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import br.com.dbccompany.coworking.Utilities.CurrencyConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository contratacaoRepository;
    @Autowired
    EspacosRepository espacoRepository;
    @Autowired
    ClientesRepository clienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public String salvar(Contratacao contratacao) {
        Espacos espacos = espacoRepository.findById(Math.toIntExact(contratacao.getEspacos().getId())).get();
        contratacao.setEspacos(espacos);

        Clientes clientes = clienteRepository.findById(Math.toIntExact(contratacao.getClientes().getId())).get();
        contratacao.setClientes(clientes);

        Long quantidade = contratacao.getQuantidade();
        Double valor = espacos.getValor();

        contratacaoRepository.save(contratacao);
        return CurrencyConvert.currencyToReais(quantidade * valor);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contratacao editar(Integer id, Contratacao contratacao){
        contratacao.setId(id);
        return contratacaoRepository.save(contratacao);
    }

    public List<Contratacao> todosContratacao(){
        return (List<Contratacao>) contratacaoRepository.findAll();
    }
}
