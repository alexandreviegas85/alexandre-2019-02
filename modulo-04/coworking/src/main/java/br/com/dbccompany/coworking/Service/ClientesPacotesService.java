package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Repository.ClientesPacotesRepository;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

    @Service
    public class ClientesPacotesService {

        @Autowired
        private ClientesPacotesRepository clientesPacotesRepository;

        @Autowired
        private ClientesRepository clientesRepository;

        @Autowired
        private PacotesRepository pacotesRepository;

        @Transactional(rollbackFor = Exception.class)
        public ClientesPacotes salvar(ClientesPacotes clientesPacotes){
            Clientes clientes = clientesRepository.findById(clientesPacotes.getIdClientes().getId()).get();
            clientesPacotes.setIdClientes(clientes);

            Pacotes pacotes = pacotesRepository.findById((clientesPacotes.getPacotes().getId())).get();
            clientesPacotes.setPacotes(pacotes);

            return clientesPacotesRepository.save(clientesPacotes);
        }

        @Transactional(rollbackFor = Exception.class)
        public ClientesPacotes editar(Integer id, ClientesPacotes clientesPacotes){
            clientesPacotes.setId(id);
            return clientesPacotesRepository.save(clientesPacotes);
        }

        public List<ClientesPacotes> todosClientesPacotes(){
            return (List<ClientesPacotes>) clientesPacotesRepository.findAll();
        }

    }
