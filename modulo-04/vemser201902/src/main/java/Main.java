import com.sun.org.apache.bcel.internal.generic.Select;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        Connection conn = Connector.connect();
       /* try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("create table PAISES(\n"
                    + "ID_PAIS INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL \n"
                        + ")").execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into PAISES(ID_PAIS, NOME)"
                + "VALUES(PAISES_SEQ.NEXTVAL, ?)");
            pst.setString(1, "ENGLAND");
            pst.executeUpdate();

            rs = conn.prepareStatement("select * from PAISES").executeQuery();
            while (rs.next()){
                System.out.println(String.format("Nome do Pais: %s", rs.getString("NOME")));
            }
        } catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro de conexão", ex);
        }*/

       /*try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'ESTADOS'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE ESTADOS(\n" +
                        "ID_ESTADO INTEGER NOT NULL PRIMARY KEY,\n" +
                        "NOME VARCHAR(100) NOT NULL,\n" +
                        "FK_ID_PAIS INTEGER NOT NULL,\n" +
                        "FOREIGN KEY (FK_ID_PAIS)\n" +
                        "REFERENCES PAISES(ID_PAIS)\n" +
                        ")").execute();
            }
            PreparedStatement pst = conn.prepareStatement("INSERT INTO ESTADOS(ID_ESTADO,NOME,FK_ID_PAIS) "
                    + "VALUES(ESTADOS_SEQ.NEXTVAL, ?, (SELECT ID_PAIS FROM PAISES WHERE NOME = 'BRASIL' AND ROWNUM = 1))");
            pst.setString(1, "BUENOS AIRES");
            pst.executeUpdate();

            rs = conn.prepareStatement("select * from ESTADOS").executeQuery();
            while (rs.next()){
                System.out.println(String.format("NOME DO ESTADO: %s", rs.getString("NOME")));
            }
        } catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro de conexão", ex);
        }*/

        /*try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'CIDADES'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE CIDADES(\n" +
                        "ID_CIDADE INTEGER NOT NULL PRIMARY KEY,\n" +
                        "NOME VARCHAR2(100) NOT NULL,\n" +
                        "FK_ID_ESTADO INTEGER NOT NULL,\n" +
                        "FOREIGN KEY (FK_ID_ESTADO)\n" +
                        "REFERENCES ESTADOS(ID_ESTADO)\n" +
                        ")").execute();
            }
            PreparedStatement pst = conn.prepareStatement("INSERT INTO CIDADES(ID_CIDADE, NOME, FK_ID_ESTADO)"
                    + " VALUES(CIDADES_SEQ.NEXTVAL, ?, (SELECT ID_ESTADO FROM ESTADOS WHERE NOME = 'NA' AND ROWNUM =1 ))");
            pst.setString(1, "BUENOS AIRES");
            pst.executeUpdate();

            rs = conn.prepareStatement("select * from CIDADES").executeQuery();
            while (rs.next()){
                System.out.println(String.format("NOME DA CIDADE: %s", rs.getString("NOME")));
            }
        } catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro de conexão", ex);
        }*/

       /* try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'BAIRROS'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE BAIRROS(\n" +
                        "ID_BAIRRO INTEGER NOT NULL PRIMARY KEY,\n" +
                        "NOME VARCHAR2(100) NOT NULL,\n" +
                        "FK_ID_CIDADE INTEGER NOT NULL,\n" +
                        "FOREIGN KEY (FK_ID_CIDADE)\n" +
                        "REFERENCES CIDADES(ID_CIDADE)\n" +
                        ")").execute();
            }
            PreparedStatement pst = conn.prepareStatement("INSERT INTO BAIRROS(ID_BAIRRO, NOME, FK_ID_CIDADE)"
                    + " VALUES(BAIRROS_SEQ.NEXTVAL, ?, (SELECT ID_CIDADE FROM CIDADES WHERE NOME = 'NA' AND ROWNUM =1))");
            pst.setString(1, "Caminito");
            pst.executeUpdate();

            rs = conn.prepareStatement("select * from BAIRROS").executeQuery();
            while (rs.next()){
                System.out.println(String.format("NOME DO BAIRRO: %s", rs.getString("NOME")));
            }
        } catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro de conexão", ex);
        }*/
    }
}