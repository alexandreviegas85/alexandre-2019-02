package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasDAO extends AbstractDAO<Correntistas> {

	@Override
	protected Class<Correntistas> getEntityClass() {
		return Correntistas.class;
	}
	
	public Correntistas parseFrom(CorrentistasDTO dto) {
		Correntistas correntistas = null;
		if(dto.getIdCorrentista() != null) {
			correntistas = buscar(dto.getIdCorrentista());
		} else {
			correntistas = new Correntistas();
		}
		correntistas.setTipo(dto.getTipoCorrentista());
		return correntistas;
	}

}
