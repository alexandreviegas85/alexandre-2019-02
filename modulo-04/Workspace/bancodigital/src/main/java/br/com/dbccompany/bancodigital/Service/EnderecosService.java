package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EnderecosDAO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Enderecos;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class EnderecosService {
	
	private static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO();
	private static final Logger LOG = Logger.getLogger(EnderecosService.class.getName());

	public void salvarEnderecos(EnderecosDTO enderecosDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();

		Enderecos enderecos = new Enderecos();
		
		try {
			Enderecos enderecosRes = ENDERECOS_DAO.buscar(1);
			if (enderecosRes == null) {
				ENDERECOS_DAO.criar(enderecos);
			} else {
				enderecos.setId(enderecosRes.getId());
				ENDERECOS_DAO.atualizar(enderecos);
			}
			if (started) {
				transaction.commit();
			}
			
			enderecosDTO.setIdEndereco(enderecos.getId());
			
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public void salvarEnderecos(Enderecos enderecos) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();

		try {
			Enderecos enderecosRes = ENDERECOS_DAO.buscar(1);
			if (enderecosRes == null) {
				ENDERECOS_DAO.criar(enderecos);
			} else {
				enderecos.setId(enderecosRes.getId());
				ENDERECOS_DAO.atualizar(enderecos);
			}
			if (started) {
				transaction.commit();
			}
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}

}
