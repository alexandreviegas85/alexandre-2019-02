package br.com.dbccompany.bancodigital.Entity;

public enum EstadosCivis {
	SOLTEIRO,
	CASADO,
	DIVORCIADO,
	VIUVO
}
