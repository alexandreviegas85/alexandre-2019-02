package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Entity.Bancos;

public class BancosDAO extends AbstractDAO<Bancos> {

	@Override
	protected Class<Bancos> getEntityClass() {
		return Bancos.class;
	}
	
	public Bancos parseFrom(BancosDTO dto) {
		Bancos cliente = null;
		if(dto.getIdBanco() != null) {
			cliente = buscar(dto.getIdBanco());
		} else {
			cliente = new Bancos();
		}
		cliente.setNome(dto.getNome());
		return cliente;
	}

}
