package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.TipoTelefones;

public class TelefonesDTO {
	
	private Integer idTelefone;
	private String numero;
	private TipoTelefones tipoTelefone;
	
	public Integer getIdTelefone() {
		return idTelefone;
	}
	public void setIdTelefone(Integer idTelefone) {
		this.idTelefone = idTelefone;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public TipoTelefones getTipoTelefone() {
		return tipoTelefone;
	}
	public void setTipoTelefone(TipoTelefones tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}
	
	

}
