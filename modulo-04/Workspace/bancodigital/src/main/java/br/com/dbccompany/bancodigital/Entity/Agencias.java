package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName = "AGENCIAS_SEQ")
public class Agencias extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name="ID_AGENCIA")
	private Integer id;
	private Integer codigo;
	private String nome;
	
	@ManyToMany(mappedBy = "agencia")
	private List<Correntistas> correntista = new ArrayList<Correntistas>();
	
	@ManyToOne
	@JoinColumn(name = "fk_id_banco")
	private Bancos banco;
	
	@ManyToOne
	@JoinColumn(name="fk_id_endereco")
	private Enderecos endereco;

	public Integer getIdAgencia() {
		return id;
	}

	public void setIdAgencia(Integer idAgencia) {
		this.id = idAgencia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public List<Correntistas> getCorrentista() {
		return correntista;
	}

	public void setCorrentista(List<Correntistas> correntista) {
		this.correntista = correntista;
	}

	public Bancos getBanco() {
		return banco;
	}

	public void setBanco(Bancos banco) {
		this.banco = banco;
	}

	public Enderecos getEndereco() {
		return endereco;
	}

	public void setEndereco(Enderecos endereco) {
		this.endereco = endereco;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}

}
