package br.com.dbccompany.bancodigital.Dto;

public class BairrosDTO {

	private Integer idBairro;
	private String nome;
	
	private CidadesDTO cidades;

	public Integer getIdBairro() {
		return idBairro;
	}

	public void setIdBairro(Integer idBairro) {
		this.idBairro = idBairro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public CidadesDTO getCidades() {
		return cidades;
	}

	public void setCidades(CidadesDTO cidades) {
		this.cidades = cidades;
	}
	
}
