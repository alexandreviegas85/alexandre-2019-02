package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "TELEFONES_SEQ", sequenceName="TELEFONES_SEQ")
public class Telefones extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator="TELEFONES_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name="ID_TELEFONE")
	private Integer id;
	
	@Column(name="NUMERO", length=100, nullable = false)
	private String numero;
	@Enumerated(EnumType.STRING)
	private TipoTelefones tipoTelefone;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "telefones_x_clientes", 
			joinColumns = { @JoinColumn(name = "fk_id_telefone")},
			inverseJoinColumns = { @JoinColumn (name = "fk_id_cliente")})
	private List<Clientes> cliente = new ArrayList<Clientes>();
	
	public Integer getIdTelefone() {
		return id;
	}
	public void setIdTelefone(Integer idTelefone) {
		this.id = idTelefone;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public TipoTelefones getTipo() {
		return tipoTelefone;
	}
	public void setTipo(TipoTelefones tipo) {
		this.tipoTelefone = tipo;
	}
	@Override
	public Integer getId() {
		return id;
	}
	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}

}
