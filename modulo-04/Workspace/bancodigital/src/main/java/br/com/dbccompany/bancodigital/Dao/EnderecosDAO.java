package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosDAO extends AbstractDAO<Enderecos> {
	BairrosDAO dao = new BairrosDAO();
	
	@Override
	protected Class<Enderecos> getEntityClass() {
		return Enderecos.class;
	}
	
	public Enderecos parseFrom(EnderecosDTO dto) {
		Enderecos endereco = null;
		if(dto.getIdEndereco() != null) {
			endereco = buscar(dto.getIdEndereco());
		} else {
			endereco = new Enderecos();
		}
		endereco.setLogradouro(dto.getLogradouro());
		endereco.setBairro(dao.parseFrom(dto.getBairros()));
		return endereco;
	}

}
