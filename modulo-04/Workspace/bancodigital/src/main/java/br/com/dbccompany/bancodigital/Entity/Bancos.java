package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "BANCOS_SEQ", sequenceName = "BANCOS_SEQ")
public class Bancos extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "BANCOS_SEQ", strategy =  GenerationType.SEQUENCE)
	@Column(name = "ID_BANCO")
	private Integer id;
	private Integer codigo;
	private String nome;
	
	@OneToMany(mappedBy = "banco", cascade = CascadeType.ALL)
	private List<Agencias> agencias = new ArrayList<Agencias>();
	
	public Integer getIdBanco() {
		return id;
	}
	public void setIdBanco(Integer idBanco) {
		this.id = idBanco;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public Integer getId() {
		return id;
	}
	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}
	
}
