package br.com.dbccompany.bancodigital.Dto;

public class EmailsDTO {
	
	private Integer idEmail;
	private String email;
	
	private ClientesDTO cliente;

	public Integer getIdEmail() {
		return idEmail;
	}

	public void setIdEmail(Integer idEmail) {
		this.idEmail = idEmail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ClientesDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClientesDTO cliente) {
		this.cliente = cliente;
	}
	
	

}
