package br.com.dbccompany.bancodigital.Entity;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name= "PAISES_SEQ", sequenceName = "PAISES_SEQ")
public class Paises extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "PAISES_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name="ID_PAIS")
	private Integer id;
	private String nome;
	
	@OneToMany(mappedBy = "pais", cascade = CascadeType.ALL)
	private List<Estados> estados = new ArrayList<Estados>();
	
	public Integer getIdPaise() {
		return id;
	}
	public void setIdPaise(Integer idPaise) {
		this.id = idPaise;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public Integer getId() {
		return id;
	}
	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}
}
