//package br.com.dbccompany.bancodigital.Service;
//
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import org.hibernate.Transaction;
//
//import br.com.dbccompany.bancodigital.Dao.MovimentacoesDAO;
//import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
//import br.com.dbccompany.bancodigital.Entity.Movimentacoes;
//
//public class MovimentacoesService {
//
//	private static final MovimentacoesDAO MOVIMENTACOES_DAO = new MovimentacoesDAO();
//	private static final Logger LOG = Logger.getLogger(MovimentacoesService.class.getName());
//	
//	public void salvarMovimentacoes(Movimentacoes movimentacoes) {
//		boolean started = HibernateUtil.beginTransaction();
//		Transaction transaction = HibernateUtil.getSession().getTransaction();
//		
//		try {
//			Movimentacoes movimentacoesRes = MOVIMENTACOES_DAO.buscar(1);
//			if(movimentacoesRes == null) {
//				MOVIMENTACOES_DAO.criar(movimentacoes);
//			}else {
//				movimentacoes.setId(movimentacoesRes.getId());
//				MOVIMENTACOES_DAO.atualizar(movimentacoes);
//			}
//			if(started) {
//				transaction.commit();
//			}
//		}catch (Exception e) {
//			transaction.rollback();
//			LOG.log(Level.SEVERE, e.getMessage(), e);
//		}
//	}
//}
