package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ")
public class Correntistas extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "CORRENTISTAS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_CORRENTISTA")
	private Integer id;

	private String razaoSocial;
	@Enumerated(EnumType.STRING)
	private TipoCorrentistas tipoCorrentista;

	private Double saldo;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "AGENCIAS_X_CORRENTISTAS", joinColumns = {
			@JoinColumn(name = "fk_id_correntista") }, inverseJoinColumns = { @JoinColumn(name = "fk_id_agencia") })
	private List<Agencias> agencia = new ArrayList<Agencias>();

	@ManyToMany(mappedBy = "correntista")
	private List<Clientes> cliente = new ArrayList<>();

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public TipoCorrentistas getTipoCorrentista() {
		return tipoCorrentista;
	}

	public void setTipoCorrentista(TipoCorrentistas tipoCorrentista) {
		this.tipoCorrentista = tipoCorrentista;
	}

	public List<Agencias> getAgencia() {
		return agencia;
	}

	public void setAgencia(List<Agencias> agencia) {
		this.agencia = agencia;
	}

	public List<Clientes> getCliente() {
		return cliente;
	}

	public void setCliente(List<Clientes> cliente) {
		this.cliente = cliente;
	}

	public Integer getIdCorrentista() {
		return id;
	}

	public void setIdCorrentista(Integer idCorrentista) {
		this.id = idCorrentista;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public TipoCorrentistas getTipo() {
		return tipoCorrentista;
	}

	public void setTipo(TipoCorrentistas tipo) {
		this.tipoCorrentista = tipo;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public boolean depositar(Double valor) {
		if (valor > 0) {
			this.saldo += valor;
			return true;
		} else
			return false;
	}

	public boolean sacar(Double valor) {
		if (this.saldo >= valor) {
			this.saldo -= valor;
			return true;
		} else
			return false;
	}

	public boolean transferir(Correntistas x, Double valor) {
		boolean valorSacado = sacar(valor);
		if(valorSacado) {
			x.setSaldo(x.getSaldo()+valor);
			return true;
		}else
			return false;
	}
	
}
