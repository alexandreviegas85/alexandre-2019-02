package br.com.dbccompany.bancodigital.Dto;

public class CidadesDTO {

	private Integer idCidade;
	private String nome;
	
	private EstadosDTO estados;

	public Integer getIdPais() {
		return idCidade;
	}

	public void setIdPais(Integer idPais) {
		this.idCidade = idPais;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EstadosDTO getEstados() {
		return estados;
	}

	public void setEstados(EstadosDTO estados) {
		this.estados = estados;
	}

	public Integer getIdCidade() {
		return idCidade;
	}

	public void setIdCidade(Integer idCidade) {
		this.idCidade = idCidade;
	}
	
	
		
}
