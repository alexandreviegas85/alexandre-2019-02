package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.TelefonesDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Telefones;

public class TelefonesService {

	private static final TelefonesDAO TELEFONES_DAO = new TelefonesDAO();
	private static final Logger LOG = Logger.getLogger(TelefonesService.class.getName());

	public void salvarTelefones(Telefones telefones) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();

		try {
			Telefones telefonesRes = TELEFONES_DAO.buscar(1);
			if (telefonesRes == null) {
				TELEFONES_DAO.criar(telefones);
			} else {
				telefones.setId(telefonesRes.getId());
				TELEFONES_DAO.atualizar(telefones);
			}
			if (started) {
				transaction.commit();
			}
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
