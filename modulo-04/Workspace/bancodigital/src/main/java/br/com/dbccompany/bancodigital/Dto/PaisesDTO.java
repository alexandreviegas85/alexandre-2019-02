package br.com.dbccompany.bancodigital.Dto;

public class PaisesDTO {
	
	private Integer idPais;
	private String nome;
	
	public Integer getIdPais() {
		return idPais;
	}
	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
