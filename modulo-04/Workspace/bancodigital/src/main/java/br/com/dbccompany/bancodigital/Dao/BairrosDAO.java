package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;

public class BairrosDAO extends AbstractDAO<Bairros> {
	CidadesDAO dao = new CidadesDAO();
	
	@Override
	protected Class<Bairros> getEntityClass() {
		return Bairros.class;
	}
	
	public Bairros parseFrom(BairrosDTO dto) {
		Bairros bairro = null;
		if(dto.getIdBairro() != null) {
			bairro = buscar(dto.getIdBairro());
		} else {
			bairro = new Bairros();
		}
		bairro.setNome(dto.getNome());
		bairro.setCidade(dao.parseFrom(dto.getCidades()));
		return bairro;
	}
	
}
