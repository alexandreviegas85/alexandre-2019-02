//package br.com.dbccompany.bancodigital.Entity;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.SequenceGenerator;
//
//@Entity
//@SequenceGenerator(allocationSize =1, name = "MOVIMENTACOES_SEQ", sequenceName = "MOVIMENTACOES_SEQ")
//public class Movimentacoes extends AbstractEntity {
//
//	private static final long serialVersionUID = 1L;
//
//	@Id
//	@GeneratedValue(generator = "MOVIMENTACOES_SEQ", strategy = GenerationType.SEQUENCE )
//	@Column(name = "ID_MOVIMENTACAO")
//	private Integer id;
//	private Double saldo;
//		
//	public Double getSaldo() {
//		return saldo;
//	}
//
//	public void setSaldo(Double saldo) {
//		this.saldo = saldo;
//	}
//
//	@Override
//	public Integer getId() {
//		return id;
//	}
//
//	@Override
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//}
