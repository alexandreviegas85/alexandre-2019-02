package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.BairrosDAO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class BairrosService {

	private static final BairrosDAO BAIRROS_DAO = new BairrosDAO();
	private static final Logger LOG = Logger.getLogger(BairrosService.class.getName());

	public void salvarBairro(BairrosDTO bairroDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();

		Bairros bairro = new Bairros();
		
		try {
			Bairros bairrosRes = BAIRROS_DAO.buscar(1);
			if (bairrosRes == null) {
				BAIRROS_DAO.criar(bairro);
			} else {
				bairro.setId(bairrosRes.getId());
				BAIRROS_DAO.atualizar(bairro);
			}
			if (started) {
				transaction.commit();
			}
			
			bairroDTO.setIdBairro(bairro.getId());
			
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public void salvarBairro(Bairros bairro) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();

		try {
			Bairros bairrosRes = BAIRROS_DAO.buscar(1);
			if (bairrosRes == null) {
				BAIRROS_DAO.criar(bairro);
			} else {
				bairro.setId(bairrosRes.getId());
				BAIRROS_DAO.atualizar(bairro);
			}
			if (started) {
				transaction.commit();
			}
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
