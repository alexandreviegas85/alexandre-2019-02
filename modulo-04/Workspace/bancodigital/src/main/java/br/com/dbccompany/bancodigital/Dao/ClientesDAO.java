package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;

public class ClientesDAO extends AbstractDAO<Clientes> {
	EnderecosDAO dao = new EnderecosDAO();
	
	@Override
	protected Class<Clientes> getEntityClass() {
		return Clientes.class;
	}

	public Clientes parseFrom(ClientesDTO dto) {
		Clientes cliente = null;
		if(dto.getIdCliente() != null) {
			cliente = buscar(dto.getIdCliente());
		} else {
			cliente = new Clientes();
		}
		cliente.setNome(dto.getNome());
		cliente.setEndereco(dao.parseFrom(dto.getEnderecos()));
		return cliente;
	}
}
