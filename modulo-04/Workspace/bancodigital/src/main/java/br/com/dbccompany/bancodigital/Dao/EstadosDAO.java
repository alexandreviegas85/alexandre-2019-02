package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class EstadosDAO extends AbstractDAO<Estados>{
	PaisesDAO dao = new PaisesDAO();
	
	@Override
	protected Class<Estados> getEntityClass() {
		
		return Estados.class;
	}
	
	public Estados parseFrom(EstadosDTO dto) {
		Estados estado = null;
		if(dto.getIdEstado() != null) {
			estado = buscar(dto.getIdEstado());
		} else {
			estado = new Estados();
		}
		estado.setNome(dto.getNome());
		estado.setPais(dao.parseFrom(dto.getPaises()));
		return estado;
	}
}
