package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.TelefonesDTO;
import br.com.dbccompany.bancodigital.Entity.Telefones;

public class TelefonesDAO extends AbstractDAO<Telefones>{

	@Override
	protected Class<Telefones> getEntityClass() {
		return Telefones.class;
	}
	
	public Telefones parseFrom(TelefonesDTO dto) {
		Telefones telefones = null;
		if(dto.getIdTelefone() != null) {
			telefones = buscar(dto.getIdTelefone());
		} else {
			telefones = new Telefones();
		}
		telefones.setNumero(dto.getNumero());
		return telefones;
	}
	

}
