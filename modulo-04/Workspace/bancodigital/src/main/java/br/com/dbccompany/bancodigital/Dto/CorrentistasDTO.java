package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.TipoCorrentistas;

public class CorrentistasDTO {

	private Integer idCorrentista;
	private Double saldo;
	private String razaoSocial;
	private String cnpj;
	private TipoCorrentistas tipoCorrentista;
	
	public Integer getIdCorrentista() {
		return idCorrentista;
	}
	public void setIdCorrentista(Integer idCorrentista) {
		this.idCorrentista = idCorrentista;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public TipoCorrentistas getTipoCorrentista() {
		return tipoCorrentista;
	}
	public void setTipoCorrentista(TipoCorrentistas tipoCorrentista) {
		this.tipoCorrentista = tipoCorrentista;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
}
