package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.CorrentistasDAO;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class CorrentistasService {

	private static final CorrentistasDAO CORRENTISTAS_DAO = new CorrentistasDAO();
	private static final Logger LOG = Logger.getLogger(CorrentistasService.class.getName());

	public void salvarCorrentistas(Correntistas correntistas) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();

		try {
			Correntistas correntistasRes = CORRENTISTAS_DAO.buscar(1);
			if (correntistasRes == null) {
				CORRENTISTAS_DAO.criar(correntistas);
			} else {
				//correntistas.setId(correntistasRes.getId());
				CORRENTISTAS_DAO.atualizar(correntistas);
			}
			if (started) {
				transaction.commit();
			}
			
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
