package br.com.dbccompany.bancodigital;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.Enderecos;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.EstadosCivis;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.Entity.TipoCorrentistas;
import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.Service.BairrosService;
import br.com.dbccompany.bancodigital.Service.BancosService;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.ClientesService;
import br.com.dbccompany.bancodigital.Service.CorrentistasService;
import br.com.dbccompany.bancodigital.Service.EnderecosService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.Service.PaisesService;

public class Main {
	
	public static void main(String[] args) {
//		PaisesService service = new PaisesService();
//		PaisesDTO paises = new PaisesDTO();
//		paises.setNome("ARGENTINA");
//		service.salvarPaises(paises);
//		
//		EstadosService serviceEst = new EstadosService();
//		EstadosDTO estado = new EstadosDTO();
//		estado.setNome("test");
//		estado.setPaises(paises);
//		serviceEst.salvarEstados(estado);
		
		
		//SERVICE
		PaisesService servicePaises = new PaisesService();
		EstadosService serviceEstados = new EstadosService();
		CidadesService serviceCidades = new CidadesService();
		BairrosService serviceBairros = new BairrosService();
		EnderecosService serviceEnderecos = new EnderecosService();
		AgenciasService serviceAgencias = new AgenciasService();
		BancosService serviceBancos = new BancosService();
		ClientesService serviceClientes = new ClientesService();
		CorrentistasService serviceCorrentistas = new CorrentistasService();
		
		
		//ENTITY
		Paises brasil = new Paises();
		Estados rs = new Estados();
		Cidades portoAlegre = new Cidades();
		Bairros navegantes = new Bairros();
		Bairros bairroDigital = new Bairros();
		Enderecos avPernambuco = new Enderecos();
		Enderecos avPernambuco1 = new Enderecos();
		Enderecos endBancos = new Enderecos();
		Bancos nuBank = new Bancos();
		Bancos bancoInter = new Bancos();
		Agencias agenciaNuBank = new Agencias();
		Agencias agenciaBancoInter = new Agencias();
		Clientes cliente01 = new Clientes();
		Clientes cliente02 = new Clientes();
		Correntistas correntista01 = new Correntistas();
		Correntistas correntista02 = new Correntistas();
		
		
		
		//PAISES
		brasil.setNome("BRASIL");
		servicePaises.salvarPaises(brasil);
		
		//ESTADOS
		rs.setNome("RS");
		rs.setPais(brasil);
		serviceEstados.salvarEstados(rs);
		
		//CIDADES
		portoAlegre.setNome("PORTO ALEGRE");
		portoAlegre.setEstado(rs);
		serviceCidades.salvarCidades(portoAlegre);
		
		//BAIRROS
		navegantes.setNome("NAVEGANTES");
		navegantes.setCidade(portoAlegre);
		serviceBairros.salvarBairro(navegantes);
		
		bairroDigital.setNome("DIGITAL");
		bairroDigital.setCidade(portoAlegre);
		serviceBairros.salvarBairro(bairroDigital);
		
		//ENDERE�OS
		avPernambuco.setLogradouro("AV PERNAMBUCO");
		avPernambuco.setNumero(1323);
		avPernambuco.setComplemento("AP 444");
		avPernambuco.setBairro(navegantes);
		serviceEnderecos.salvarEnderecos(avPernambuco);
		
		avPernambuco1.setLogradouro("AV PERNAMBUCO");
		avPernambuco1.setNumero(1323);
		avPernambuco1.setComplemento("AP 555");
		avPernambuco1.setBairro(navegantes);
		serviceEnderecos.salvarEnderecos(avPernambuco1);
		
		endBancos.setLogradouro("AV DIGITAL");
		endBancos.setNumero(999);
		endBancos.setBairro(bairroDigital);
		serviceEnderecos.salvarEnderecos(endBancos);
		
		//BANCOS
		nuBank.setNome("NU BANK");
		nuBank.setCodigo(260);
		serviceBancos.salvarBanco(nuBank);
		
		bancoInter.setNome("BANCO INTER");
		bancoInter.setCodigo(77);
		serviceBancos.salvarBanco(bancoInter);
		
		//AGENCIAS
		agenciaNuBank.setCodigo(1001);
		agenciaNuBank.setNome("DIGITAL NU BANK");
		agenciaNuBank.setBanco(nuBank);
		agenciaNuBank.setEndereco(endBancos);
		serviceAgencias.salvarAgencias(agenciaNuBank);
		
		agenciaBancoInter.setCodigo(1001);
		agenciaBancoInter.setNome("DIGITAL BANCO INTER");
		agenciaBancoInter.setBanco(bancoInter);
		agenciaBancoInter.setEndereco(endBancos);
		serviceAgencias.salvarAgencias(agenciaBancoInter);
		
		//LIST
		List<Clientes> listCliente01 = new ArrayList<Clientes>();
		listCliente01.add(cliente01);
		List<Clientes> listCliente02 = new ArrayList<Clientes>();
		listCliente02.add(cliente02);
		List<Agencias> listAgencia01 = new ArrayList<Agencias>();
		listAgencia01.add(agenciaNuBank);
		List<Agencias> listAgencia02 = new ArrayList<Agencias>();
		listAgencia02.add(agenciaBancoInter);
		List<Correntistas> listCorrentistas01 = new ArrayList<Correntistas>();
		listCorrentistas01.add(correntista01);
		List<Correntistas> listCorrentistas02 = new ArrayList<Correntistas>();
		listCorrentistas02.add(correntista02);

		//CLIENTES
		cliente01.setNome("CEBOLINHA");
		cliente01.setRg("123456789");
		cliente01.setCpf("00755597800");
		cliente01.setDataNascimento("02/10/1997");
		cliente01.setEndereco(avPernambuco1);
		cliente01.setEstadoCivil(EstadosCivis.SOLTEIRO);
		cliente01.setCorrentista(listCorrentistas01);
		serviceClientes.salvarClientes(cliente01);
		
		cliente02.setNome("GEROMEL");
		cliente02.setRg("987654321");
		cliente02.setCpf("00766688855");
		cliente02.setDataNascimento("20/01/1995");
		cliente02.setEndereco(avPernambuco);
		cliente02.setEstadoCivil(EstadosCivis.SOLTEIRO);
		cliente02.setCorrentista(listCorrentistas02);
		serviceClientes.salvarClientes(cliente02);
		
		//CORRENTISTAS
		
		correntista01.setSaldo(0.00);
		correntista01.depositar(100.00);
		correntista01.setTipo(TipoCorrentistas.PF);
		correntista01.setCliente(listCliente01);
		correntista01.setAgencia(listAgencia01);
		serviceCorrentistas.salvarCorrentistas(correntista01);

		correntista02.setSaldo(0.00);
		correntista02.depositar(100.00);
		correntista02.setTipo(TipoCorrentistas.PF);
		correntista02.setCliente(listCliente02);
		correntista02.setAgencia(listAgencia02);
		serviceCorrentistas.salvarCorrentistas(correntista02);
		
		//TRANSFER�NCIA
		System.out.println("saldo correntistas 1 " + correntista01.getSaldo());
		System.out.println("saldo correntistas 2 " + correntista02.getSaldo());
		
		System.out.println("transferencia correntista1 para correntista2");
		correntista01.transferir(correntista02, 100.00);
		serviceCorrentistas.salvarCorrentistas(correntista01);
		serviceCorrentistas.salvarCorrentistas(correntista02);
		System.out.println("*********************************************");
		System.out.println("saldo atual correntistas 1 " + correntista01.getSaldo());
		System.out.println("saldo atual correntistas 2 " + correntista02.getSaldo());
		
		System.exit(0);
		
//		ClientesService service1 = new ClientesService();
//		ClientesDTO cliente = new ClientesDTO();
//		cliente.setNome("Jo�o");
//		service1.salvarClientes(cliente);
	}
	
//	public static void main(String[] args) {
//	
//		Session session = null;
//		Transaction transaction = null;
//		try {
//			session = HibernateUtil.getSession();
//			transaction = session.beginTransaction();
//			
//			Paises paises = new Paises();
//			paises.setNome("BRASIL");
//			
//			session.save(paises);
//			
//			//session.createQuery("select * from paises;").executeUpdate();
//			
//			Criteria criteria = session.createCriteria(Paises.class);
//			criteria.createAlias("nome", "nome_paises");
//			criteria.add(
//					Restrictions.isNotNull("nome")					
//					);
//			
//			List<Paises> lstPaises = criteria.list();
//			
//			transaction.commit();
//		}catch (Exception e) {
//			if(transaction !=null) {
//				transaction.rollback();
//			}LOG.log(Level.SEVERE,e.getMessage(), e);
//			System.exit(1);
//		}finally {
//			if(session !=null) {
//				session.close();
//			}
//		}
//		System.exit(0);	
//	}
	
}
