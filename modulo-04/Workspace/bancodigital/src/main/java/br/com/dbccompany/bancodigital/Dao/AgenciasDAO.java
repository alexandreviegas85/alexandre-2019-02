package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;

public class AgenciasDAO extends AbstractDAO<Agencias> {
	EnderecosDAO dao = new EnderecosDAO();
	BancosDAO daoB = new BancosDAO();

	@Override
	protected Class<Agencias> getEntityClass() {
		return Agencias.class;
	}
	public Agencias parseFrom(AgenciasDTO dto) {
		Agencias agencia = null;
		if(dto.getIdAgencia() != null) {
			agencia = buscar(dto.getIdAgencia());
		} else {
			agencia = new Agencias();
		}
		agencia.setNome(dto.getNome());
		agencia.setEndereco(dao.parseFrom(dto.getEnderecos()));
		agencia.setBanco(daoB.parseFrom(dto.getBancos()));
		return agencia;
	}

}
