package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.EstadosCivis;

public class ClientesDTO {

	private Integer idCliente;
	private String nome;
	private String cpf;
	private String rg;
	private String dataNascimento;
	private String conjuge;
	private EstadosCivis estadoCivil;
	
	private EnderecosDTO enderecos;

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public EstadosCivis getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadosCivis estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public EnderecosDTO getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(EnderecosDTO enderecos) {
		this.enderecos = enderecos;
	}
	
}
