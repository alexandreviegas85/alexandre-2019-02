package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.AgenciasDAO;
import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class AgenciasService {
	
	private static final AgenciasDAO AGENCIAS_DAO = new AgenciasDAO();
	private static final Logger LOG = Logger.getLogger(AgenciasService.class.getName());
	
	public void salvarAgencias(AgenciasDTO agenciasDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Agencias agencias = new Agencias();
		
		try {
			Agencias agenciasRes = AGENCIAS_DAO.buscar(1);
			if(agenciasRes == null) {
				AGENCIAS_DAO.criar(agencias);
			}else {
				agencias.setId(agenciasRes.getId());
				AGENCIAS_DAO.atualizar(agencias);
			}
			if(started) {
				transaction.commit();
			}
			
			agenciasDTO.setIdAgencia(agencias.getId());
			
		}catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public void salvarAgencias(Agencias agencias) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Agencias agenciasRes = AGENCIAS_DAO.buscar(1);
			if(agenciasRes == null) {
				AGENCIAS_DAO.criar(agencias);
			}else {
				agencias.setId(agenciasRes.getId());
				AGENCIAS_DAO.atualizar(agencias);
			}
			if(started) {
				transaction.commit();
			}
		}catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
