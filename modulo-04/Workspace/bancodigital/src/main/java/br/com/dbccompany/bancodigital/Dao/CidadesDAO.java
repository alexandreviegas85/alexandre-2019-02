package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;

public class CidadesDAO extends AbstractDAO<Cidades> {
	EstadosDAO dao = new EstadosDAO();
	
	@Override
	protected Class<Cidades> getEntityClass() {
		return Cidades.class;
	}
	
	public Cidades parseFrom(CidadesDTO dto) {
		Cidades cidade = null;
		if(dto.getIdPais() != null) {
			cidade = buscar(dto.getIdPais());
		} else {
			cidade = new Cidades();
		}
		cidade.setNome(dto.getNome());
		cidade.setEstado(dao.parseFrom(dto.getEstados()));
		return cidade;
	}
}
