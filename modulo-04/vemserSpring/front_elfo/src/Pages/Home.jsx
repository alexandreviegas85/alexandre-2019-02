import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import './Home.css'


export default class Home extends Component {

    logout() {
        localStorage.removeItem('Authorization')
    }

    render() {
        return (
            <React.Fragment>
                <button type='button' className='buttonLogout' onClick={this.logout.bind(this)}>
                    <Link className='textCard' to='/'>LOGOUT</Link>
                </button>
                <div className='pg container'>
                    <div className=''>
                        <Link className='card textCard' to='/elfos'>ELFOS</Link>
                        <Link className='card textCard' to='/newElfo'>NOVO ELFOS</Link>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}