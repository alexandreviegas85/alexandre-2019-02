import React, { Component } from 'react'
import * as axios from 'axios';
import './Elfos.css'

export default class Elfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ListaElfos: []
        }
    }
    componentDidMount() {
        const header = {
            headers: { Authorization: localStorage.getItem('Authorization') }
        }
        axios.get('http://localhost:8080/api/elfo/', header).then(resp => {
            console.log(resp)
            const respLista = resp.data;
            this.setState({
                ListaElfos: respLista
            })
        })
    }

    render() {
        const { ListaElfos } = this.state
        return (
            <React.Fragment>
                <h1 className="elfocontainer">LISTA ELFOS</h1>
                {ListaElfos.map( e => {
                    return (
                        <React.Fragment >
                            <div className="elfocard">
                                <p className="">Id: {e.id}</p><br />
                                <p className="">Nome: {e.nome}</p><br />
                                <p className="">Experiencia: {e.experiencia} </p><br />
                                <p className="">Vida: {e.vida} </p><br />
                                <p className="">Dano: {e.dano} </p><br />
                            </div>
                        </React.Fragment>
                    )
                })}
            </React.Fragment>
        )
    }
}