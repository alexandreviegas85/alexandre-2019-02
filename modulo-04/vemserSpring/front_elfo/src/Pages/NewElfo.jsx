import React, { Component } from 'react'
import * as axios from 'axios';
import './NewElfo.css'

export default class NewElfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nome: "",
            vida: "",
            experiencia: "",
            dano: ""
        }
        this.trocaValoresState = this.trocaValoresState.bind(this);
        this.newElfo = this.newElfo.bind(this);
    }

    trocaValoresState(e) {
        const {name, value} = e.target
        this.setState({
            [name]: value
        })
    }

    newElfo(e){
        e.preventDefault();
        const header = {
            headers: { Authorization: localStorage.getItem('Authorization') }
        }
        const { nome, vida, experiencia, dano } = this.state
        if (nome) {
            axios.post('http://localhost:8080/api/elfo/novo/',
            {
                nome: this.state.nome,
                vida: this.state.vida,
                experiencia: this.state.experiencia,
                dano: this.state.dano
            }, header).then(resp => {
                this.props.history.push('/')
                console.log(resp)
             })
        }
    }

    render(){
        return(
            <React.Fragment>
                <h1 className="elfocontainer">NOVO ELFO</h1>
                <div className="">
                    <input  type="text" name="nome"  id="nome"  placeholder="Nome" onChange={this.trocaValoresState}/>
                    <input  type="text" name="vida" id="vida" placeholder="Vida 00.0" onChange={this.trocaValoresState}/>
                    <input  type="text" name="experiencia"  id="experiencia"  placeholder="Experiencia" onChange={this.trocaValoresState}/>
                    <input  type="text" name="dano" id="dano" placeholder="Dano" onChange={this.trocaValoresState}/>
                    <button type="button" className="buttonNewElfo" onClick={this.newElfo}>CRIAR</button>
                </div>
            </React.Fragment>
        )
    }

}