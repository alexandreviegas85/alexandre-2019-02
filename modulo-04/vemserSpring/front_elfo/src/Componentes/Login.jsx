import React, { Component } from 'react';
import * as axios from 'axios';
import './Login.css'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    trocaValoresState(e) {
        const { name, value } = e.target;
        this.setState(
            {
                [name]: value
            }
        )
    }

    logar(e) {
        e.preventDefault();
        const { username, password } = this.state
        if (username && password) {
            axios.post('http://localhost:8080/login', {
                username: this.state.username,
                password: this.state.password
            }).then(response => {
                localStorage.setItem('Authorization', response.headers.authorization);
                this.props.history.push('/')
                console.log(response)
            })
            
        }
    }

    render() {
        return (
            <React.Fragment>
                    <div className='logar container'>
                        <h2>Login</h2>
                        <input type='text' name='username' id='name' placeholder='admin' onChange={this.trocaValoresState} />
                        <input type='password' name='password' id='password' placeholder='123456' onChange={this.trocaValoresState} />
                        <button onClick={this.logar.bind(this)}>Logar</button>
                    </div>
            </React.Fragment>
        )
    }
}