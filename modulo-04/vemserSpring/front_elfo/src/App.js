import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import PrivateRoute from './Componentes/PrivateRoute'
import Login from './Componentes/Login'
import Home from './Pages/Home'
import Elfos from './Pages/Elfos'
import NewElfo from './Pages/NewElfo'
import './App.css';

class App extends React.Component{
  render(){
    return(
      <div>
        <Router>
          <React.Fragment>
            <PrivateRoute path="/" exact component ={Home}/>
            <Route path="/login" component={Login}/>
            <Route path="/elfos" component={Elfos} />
            <Route path="/newElfo" component={NewElfo}/>
          </React.Fragment>
        </Router>
      </div>
    )
  }
}

export default App;
