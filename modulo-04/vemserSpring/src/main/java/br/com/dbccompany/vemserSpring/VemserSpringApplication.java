package br.com.dbccompany.vemserSpring;

import br.com.dbccompany.vemserSpring.Entity.*;
import br.com.dbccompany.vemserSpring.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VemserSpringApplication {

//	@Autowired
//	ElfoService elfoService;
//	@Autowired
//    DwarfService dwarfService;
//	@Autowired
//	InventarioService inventarioService;
//	@Autowired
//	InventarioXItemService inventarioXItemService;
//	@Autowired
//	ItemService itemService;

	public static void main(String[] args) {
		SpringApplication.run(VemserSpringApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args -> {
//			Elfo elfo = new Elfo();
//			elfo.setNome("Legolas");
//			elfoService.salvar(elfo);
//
//			Elfo elfo1 = new Elfo();
//			elfo1.setNome("Elfo");
//			elfoService.salvar(elfo1);
//
//			elfoService.excluir(elfo1);
//
//            Dwarf dwarf = new Dwarf();
//            dwarf.setNome("Dwarf");
//            dwarfService.salvar(dwarf);
//
//			Item espadaElfo = new Item();
//			espadaElfo.setDescricao("Espada");
//			itemService.salvar(espadaElfo);
//
//			Inventario inventarioElfo = new Inventario();
//			inventarioElfo.setPersonagem(elfo);
//			inventarioElfo.setTamanho(1);
//			inventarioService.salvar(inventarioElfo);
//
//			InventarioXItem inventarioXItem = new InventarioXItem();
//			inventarioXItem.setInventario(inventarioElfo);
//			inventarioXItem.setItem(espadaElfo);
//			inventarioXItem.setQuantidade(1);
//			inventarioXItemService.salvar(inventarioXItem);
		};
	}
}
