package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Elfo;
import br.com.dbccompany.vemserSpring.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/elfo")
public class ElfoController {

    @Autowired
    ElfoService elfoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Elfo> todosElfos(){
        return elfoService.todosElfos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Elfo novoElfo(@RequestBody Elfo elfo){
        return  elfoService.salvar(elfo);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Elfo editarElfo(@PathVariable Integer id, @RequestBody Elfo elfo){
        return elfoService.editar(id, elfo);
    }

  }
