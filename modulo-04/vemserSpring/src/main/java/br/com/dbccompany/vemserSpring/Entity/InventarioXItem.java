package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;

@Entity
public class InventarioXItem {

    @Id
    @SequenceGenerator(allocationSize = 1,name = "QTDINVENTARIOITEM_SEQ", sequenceName = "QTDINVENTARIOITEM_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QTDINVENTARIOITEM_SEQ")
    @Column(name = "ID_QTDINVENTARIOITEM")
    private Integer id;
    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    @ManyToOne
    @JoinColumn(name = "FK_ID_INVENTARIO")
    private Inventario inventario;

    @ManyToOne
    @JoinColumn(name = "FK_ID_ITEM")
    private Item item;

    public Integer getId() {
        return id;
    }

    public InventarioXItem setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public InventarioXItem setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
        return this;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public InventarioXItem setInventario(Inventario inventario) {
        this.inventario = inventario;
        return this;
    }

    public Item getItem() {
        return item;
    }

    public InventarioXItem setItem(Item item) {
        this.item = item;
        return this;
    }
}
