package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Personagem {

    @Id
    @Column(name = "ID_PERSONAGEM")
    @SequenceGenerator( allocationSize = 1, name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue(generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "NOME")
    private String nome;
    @Column(name = "EXPERIENCIA")
    private Integer experiencia = 0;
    @Column(name = "VIDA")
    private Double vida = 100.00;
    @Column(name = "DANO")
    private Double dano = 0.00;

    @OneToOne(mappedBy = "personagem")
    private Inventario inventario;

    @Enumerated(EnumType.STRING)
    private TipoPersonagem tipoPersonagem;

    @Enumerated(EnumType.STRING)
    private Status status = Status.RECEM_CRIADO;

    public Integer getId() {
        return id;
    }

    public Personagem setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public Personagem setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public Personagem setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
        return this;
    }

    public Double getVida() {
        return vida;
    }

    public Personagem setVida(Double vida) {
        this.vida = vida;
        return this;
    }

    public Double getDano() {
        return dano;
    }

    public Personagem setDano(Double dano) {
        this.dano = dano;
        return this;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public Personagem setInventario(Inventario inventario) {
        this.inventario = inventario;
        return this;
    }

    public TipoPersonagem getTipoPersonagem() {
        return tipoPersonagem;
    }

    protected Personagem setTipoPersonagem(TipoPersonagem tipoPersonagem) {
        this.tipoPersonagem = tipoPersonagem;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public Personagem setStatus(Status status) {
        this.status = status;
        return this;
    }
}


