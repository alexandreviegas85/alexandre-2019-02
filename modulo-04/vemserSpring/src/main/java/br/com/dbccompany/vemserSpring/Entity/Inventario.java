package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Inventario {

    @Id
    @SequenceGenerator(allocationSize = 1,name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ")
    @GeneratedValue(generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_INVENTARIO")
    private Integer id;
    private Integer tamanho;

    @OneToOne
    @JoinColumn(name = "FK_ID_PERSONAGEM")
    private Personagem personagem;

    @OneToMany(mappedBy = "inventario", cascade = CascadeType.ALL)
    private List<InventarioXItem> inventarioXItems = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    public Personagem getPersonagem() {
        return personagem;
    }

    public void setPersonagem(Personagem personagem) {
        this.personagem = personagem;
    }

    public List<InventarioXItem> getInventarioXItems() {
        return inventarioXItems;
    }

    public void setInventarioXItems(List<InventarioXItem> inventarioXItems) {
        this.inventarioXItems = inventarioXItems;
    }
}
