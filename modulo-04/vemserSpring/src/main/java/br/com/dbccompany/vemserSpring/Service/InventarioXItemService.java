package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Repository.InventarioXItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class InventarioXItemService {

    @Autowired
    private InventarioXItemRepository inventarioXItemRepository;

    @Transactional(rollbackFor = Exception.class)
    public InventarioXItem salvar(InventarioXItem inventarioXItem){
        return inventarioXItemRepository.save(inventarioXItem);
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioXItem editar(Integer id, InventarioXItem inventarioXItem){
        inventarioXItem.setId(id);
        return inventarioXItemRepository.save(inventarioXItem);
    }

    public List<InventarioXItem> todosInventariosXItens(){
        return (List<InventarioXItem>) inventarioXItemRepository.findAll();
    }

}
