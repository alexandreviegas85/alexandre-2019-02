package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Inventario;
import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Entity.Item;
import br.com.dbccompany.vemserSpring.Repository.InventarioRepository;
import br.com.dbccompany.vemserSpring.Repository.ItemRepository;
import br.com.dbccompany.vemserSpring.Service.InventarioXItemService;
import br.com.dbccompany.vemserSpring.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/InventarioXItem")
public class InventarioXItemController {

    @Autowired
    InventarioXItemService inventarioXItemService;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    InventarioRepository inventarioRepository;

    @GetMapping(value = "/")
    @ResponseBody
    public List<InventarioXItem> todosInventarioXItems(){
        return inventarioXItemService.todosInventariosXItens();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public InventarioXItem novoInventarioXItem(@RequestBody InventarioXItem inventarioXItem){
        Item itemObject = inventarioXItem.getItem();
        Inventario inventarioObject = inventarioXItem.getInventario();
        if(itemObject.getId() == null){
            Item item = itemRepository.save(itemObject);
        }

        if (inventarioObject.getId() == null){
            inventarioObject = inventarioRepository.save(inventarioObject);
        }

        return  inventarioXItemService.salvar(inventarioXItem);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public InventarioXItem editarInventarioXItem(@PathVariable Integer id, @RequestBody InventarioXItem inventarioXItem){
        return  inventarioXItemService.editar(id, inventarioXItem);
    }
}
