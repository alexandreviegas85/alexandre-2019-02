
public class Dwarf extends Personagem {
   

    public Dwarf(String nome) {
       super(nome);
       this.inventario.adicionarItem(new Item (1,"Escudo"));
 
       this.vida=110.0;
    }
   

        
    //@Override
    public void calcularDano(){
        if(this.inventario.buscarItem("Escudo") != null){
            this.vida = this.vida>5 ? this.vida-5.0 : 0.0;
        }else {
            this.vida = this.vida>10 ? this.vida-10.0 : 0.0;
        }
    }

    public void sofreDano() {
        if(podeSofrerDano()){
            calcularDano();
            if(this.vida ==0.0){
                this.status = Status.MORTO;
            } 
        }
    }

    public String imprimirResultado(){
    return "dwarf";
    }
    
}
