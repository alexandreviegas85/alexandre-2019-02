public class Item {
	protected int quantidade;
	protected String descricao;
	
	public Item(int quantidade, String descricao) {
		this.quantidade = quantidade;
		this.descricao = descricao;
	}

	public int getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void acrescentaMaisUm() {
		this.quantidade++;
	}

	public String getDescricao() {
		return this.descricao;
	}
	
	@Override
	public boolean equals(Object obj){
	   Item outroItem = (Item)obj;
	   return this.quantidade == outroItem.getQuantidade() && this.descricao.equals(outroItem.getDescricao());
	}

	
	
}
