
public class ExercitoQueAtaca extends ExercitoElfo
{
    private EstrategiaDeAtaque estrategia;

    public ExercitoQueAtaca(EstrategiaDeAtaque estrategia){
        this.estrategia = estrategia;
    }

    public void trocarEstrategia (EstrategiaDeAtaque estrategia){
        this.estrategia = estrategia; 
    }
}
