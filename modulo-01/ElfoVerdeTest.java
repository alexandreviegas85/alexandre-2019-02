

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    
    private final double DELTA = 1e-9; 
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentar2XP(){
        Dwarf novoAnao = new Dwarf("Dain");
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        novoElfo.atirarFlechaNoAnao(novoAnao);
        assertEquals(2,novoElfo.getExperiencia());
        assertEquals(1,novoElfo.getQtdFlecha());
        assertEquals(105.0, novoAnao.getVida(),DELTA);
        
    } 
    
    @Test
    public void adicionaItemComDescricaoValida(){
        
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arcoDeVidro = new Item(1,"Arco de Vidro");
        novoElfo.ganharItem(arcoDeVidro);
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(2,"Flecha"), inventario.obterItem(0));
        assertEquals(new Item(1,"Arco"), inventario.obterItem(1));
        assertEquals(arcoDeVidro, inventario.getItens().get(2));    
    } 
    
    @Test
    public void perdeItemComDescricaoValida(){
        
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arcoDeVidro = new Item(1,"Arco de Vidro");
        novoElfo.ganharItem(arcoDeVidro);
        novoElfo.perderItem(arcoDeVidro);
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(2,"Flecha"), inventario.obterItem(0));
        assertEquals(new Item(1,"Arco"), inventario.obterItem(1));
        assertNull(inventario.buscarItem("Arco de Vidro"));     
    } 
    
    
}
