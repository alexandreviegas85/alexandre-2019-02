import java.util.ArrayList;
import java.util.Arrays;

public class ElfoDaLuz extends Elfo
{
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList(
                "Espada de Galvorn"
            )
        );

    private final int QTD_VIDA_GANHA = 10;
    protected int numeroVezesAtaque = 0;

    public ElfoDaLuz(String nome){
        super(nome);
        this.qtdExperiencioPorAtaque =1;
        this.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
        this.qtdDano = 21.0; 
        this.ganharVidaDuranteAtaque = 10;

    } 

    public Item getEspada(){
        return this.getInventario().buscarItem(DESCRICOES_OBRIGATORIAS.get(0));
    }

    public void perderItem(Item item){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder){
            super.perderItem(item);
        }
    }

    private boolean devePerderVida(){
        return numeroVezesAtaque%2==1; 
    }

    private void ganharVida(){
        vida+= QTD_VIDA_GANHA;
    }

    public void atacarComEspada(Dwarf dwarf){
        if(getEspada().getQuantidade()>0){
            numeroVezesAtaque++;
            dwarf.sofreDano();
            this.aumentarXP();
            if (devePerderVida())
            elfoSofrerDano();
            else ganharVida();
        }
    } 

}
