import java.util.ArrayList;

public class Inventario {
    private ArrayList<Item> itens = new ArrayList<>();
    private ArrayList<Item> itensDesativados = new ArrayList<>();

    public Inventario(int qtd){
        this.itens= new ArrayList<>(qtd);
    }

    public Inventario(){ 
        this(100);
    }

    public ArrayList<Item> getItensDesativados() {
        return itensDesativados;
    }

    public ArrayList<Item> getItens() {
        return itens;
    }

    public void desequiparItem(String nome){
        for(int i=0; i< this.itens.size();i++){
            if(this.itens.get(i).getDescricao().equals(nome)){
                itensDesativados.add(itens.get(i));
                itens.remove(i);
            }
        }
    }

    public void equiparItem(String nome){
        for(int i=0; i< this.itensDesativados.size();i++){
            if(this.itensDesativados.get(i).getDescricao().equals(nome)){
                itens.add(itensDesativados.get(i));
                this.itensDesativados.remove(i);
            }
        }
    }

    public Item obterItemDesativados(int posicao) {
        if(posicao>= this.itensDesativados.size()){
            return null;
        }
        return this.itensDesativados.get(posicao);
    }

    public void adicionarItem(Item item) {
        this.itens.add(item);
    }

    public void removerItem(Item item) {
        this.itens.remove(item);
    }

    public Item obterItem(int posicao) {
        if(posicao>= this.itens.size()){
            return null;
        }
        return this.itens.get(posicao);
    }

    public String retornarListaNomeItem() {
        StringBuilder descricoes = new StringBuilder();
        for (int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get(i);
            if (item != null) {
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }
        }
        return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length() - 1)) : descricoes.toString());
    }

    public Item retornaItemComMaiorQtd() {
        int indice = 0, maiorQuantidade = 0;
        for (int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get(i);

            if (itens.get(i).getQuantidade() > maiorQuantidade) {

                maiorQuantidade = item.getQuantidade();
                indice = i;
            }
        }

        return this.itens.size() > 0 ? this.itens.get(indice) : null;
    }

    public Item buscarItem(String nome){
        for (Item itemAtual : this.itens){
            boolean encontrei = itemAtual.getDescricao().equals(nome);
            if(encontrei){
                return itemAtual;
            }
        }
        return null;
    }

    /*public Item buscar(String nome) {
        int aux = 0;
        for (int i = 0; i < this.itens.size(); i++) {
            if (this.itens.get(i).getDescricao().equals(nome)) {
                aux = i;
            }
        }
        return itens.get(aux);
    }*/

    public String retornarListaNomeInvertida() {
        StringBuilder descricoes = new StringBuilder();
        int aux = itens.size()-1;
        for (int i = aux; i >= 0; i--) {
            Item item = this.itens.get(i);
            if (item != null) {
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }
        }
        return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length() - 1)) : descricoes.toString());
    }

    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }

    public void ordenarItens(TipoOrdenacao ordenacao){
        for(int i =0; i< this.itens.size(); i++){
            for(int j =0; j<this.itens.size()-1; j++){
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j+1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? 
                        atual.getQuantidade() > proximo.getQuantidade():
                    atual.getQuantidade() < proximo.getQuantidade();
                if(deveTrocar){
                    Item itemTrocado = atual;
                    this.itens.set(j,proximo);
                    this.itens.set(j+1,itemTrocado);
                }
            }
        }
    }

    public ArrayList<Item>inverter(){
        ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
        for (int i = this.itens.size()-1; i>=0;i--){
            listaInvertida.add(this.itens.get(i));
        }
        return listaInvertida;
    }

    public void ganharItem(Item item) {
        this.itens.add(item);
    }

    public void perderItem(Item item) {
       this.itens.remove(item);

    }

}
