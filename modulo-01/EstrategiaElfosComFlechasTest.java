
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaElfosComFlechasTest
{
    @Test
    public void exercitoOrdenadoDescendente30PorCentoElfosNoturnos(){
        EstrategiaElfosComFlechas estrategia = new EstrategiaElfosComFlechas();
        Elfo night1 = new ElfoNoturno("Noturno1");
        Elfo night2 = new ElfoNoturno("Noturno2");
        Elfo night3 = new ElfoNoturno("Noturno3");
        Elfo night4 = new ElfoNoturno("Noturno4");
        Elfo green1 = new ElfoVerde("Verde1");
        Elfo green2 = new ElfoVerde("Verde2");
        Elfo green3 = new ElfoVerde("Verde3");

        green1.ganharItem(new Item(4, "Flecha"));
        green2.ganharItem(new Item(7, "Flecha"));
        green3.ganharItem(new Item(3, "Flecha"));
        night1.ganharItem(new Item(8, "Flecha"));
       
        ArrayList<Elfo> elfosEnviados= new ArrayList<>(
                Arrays.asList(night1, night2, green1, green2, night3, night4, green3));

        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
                Arrays.asList(night1, green2, green1, green3));

        ArrayList<Elfo> elfosResultado = estrategia.EstrategiaElfosComMaisFlechasPrimeiro(elfosEnviados);

        assertEquals(elfosEsperados,elfosResultado);
    }
}
