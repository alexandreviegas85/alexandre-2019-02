
public abstract class Personagem
{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected int experiencia;
    protected double vida;
    protected double qtdDano = 0;
    {
        status = Status.RECEM_CRIADO;
        experiencia =0;
        this.inventario = new Inventario();
    }

    protected Personagem(String nome){
        this.nome = nome;
    }

    protected Personagem(){

    }

    protected Inventario getInventario(){
        return this.inventario;
    }

    protected String getNome() {
        return this.nome;
    }

    protected void setNome(String nome) {
        this.nome = nome;
    }

    protected double getVida() {
        return this.vida;
    }

    protected Status getStatus() {
        return this.status;
    }

    protected void setStatus(Status status) {
        this.status = status;
    }

    protected int getExperiencia() {
        return this.experiencia;
    }

    protected void ganharItem(Item item) {
        this.inventario.adicionarItem(item);
    }

    protected void perderItem(Item item) {
        this.inventario.removerItem(item);

    }

    protected boolean podeSofrerDano() {
        return this.vida>0;
    }

    protected abstract String imprimirResultado();

}

