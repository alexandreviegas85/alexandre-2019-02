import java.util.*;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;
import java.lang.*;

public class ExercitoElfo  
{   
    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(
                ElfoVerde.class,
                ElfoNoturno.class)
        );

   // private ArrayList<Elfo> elfoVerde = new ArrayList<>();
   // private ArrayList<Elfo> elfoNoturno = new ArrayList<>();
    private ArrayList<Elfo> exercito = new ArrayList<>(); 
  //  private ArrayList<Elfo> listaOrdenada = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();

    /*public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        boolean status = !atacantes.contains(Status.MORTO);
        if(status){
            for(int i=0;i<atacantes.size();i++){
                if(atacantes.get(i) instanceof ElfoVerde){
                    elfoVerde.add(exercito.get(i));}
                if(atacantes.get(i) instanceof ElfoNoturno){
                    elfoNoturno.add(exercito.get(i));}
                    
            }
            listaOrdenada.addAll(elfoVerde);
            listaOrdenada.addAll(elfoNoturno);
        }
        return listaOrdenada;
    }*/
    
    public void alistarElfo(Elfo elfo){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if(podeAlistar){
            exercito.add(elfo);
            ArrayList<Elfo> elfoDoStatus = porStatus.get(elfo.getStatus());
            if(elfoDoStatus == null){
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(),elfoDoStatus);
            }
            elfoDoStatus.add(elfo);
        }
        /*if(elfo instanceof ElfoVerde || elfo instanceof ElfoNoturno){
        exercito.add(elfo);
        }*/
    }

    public ArrayList<Elfo> buscar(Status status){
        return this.porStatus.get(status);
    }

    public ArrayList<Elfo> getElfos(){
        return this.exercito;
    }

    /*public String buscarElfoPorStatus(Status status) {
    StringBuilder lista = new StringBuilder();
    for (int i = 0; i < this.exercito.size(); i++) {
    Elfo elfo = this.exercito.get(i);
    if(exercito.get(i).getStatus().equals(status)) {
    lista.append(elfo.getNome());
    lista.append(",");
    }
    }
    return (lista.length() > 0 ? lista.substring(0, (lista.length() - 1)) : lista.toString());
    }*/

    public ArrayList<Elfo> getExercito(){
        return this.exercito;
    }
    
  }
