

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


public class EstrategiaPriorizandoElfosVerdesTest
{
    @Test
    public void exercitoEmbaralhadoPriorizaAtaqueComElfosVerdes(){
        EstrategiaPriorizandoElfosVerdes estrategia = new EstrategiaPriorizandoElfosVerdes();
        Elfo night1 = new ElfoNoturno("Noturno1");
        Elfo night2 = new ElfoNoturno("Noturno2");
        Elfo night3 = new ElfoNoturno("Noturno3");
        Elfo green1 = new ElfoVerde("Verde1");
        Elfo green2 = new ElfoVerde("Verde2");
        Elfo green3 = new ElfoVerde("Verde3");
        
        ArrayList<Elfo> elfosEnviados = new ArrayList<>(
            Arrays.asList(night1, night2, green1, night3, green2));
        
        
        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
            Arrays.asList(green1, green2, night1, night2, night3));
            
        ArrayList<Elfo> elfosResultado = estrategia.getOrdemDeAtaque(elfosEnviados);
        assertEquals(elfosEnviados,elfosEsperados);
    }
    
}
