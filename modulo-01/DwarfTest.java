
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest
{
    
    private final double DELTA = 1e-9; 
    
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarf = new Dwarf("Dain");
        assertEquals(110.0, dwarf.getVida(), .000001 /* ou 1e-9 */);
    }

    @Test
    public void dwarfNasceCom110Vida(){
        Dwarf dwarf = new Dwarf("Dain");
        dwarf.sofreDano();
        assertEquals (105.0, dwarf.getVida(), DELTA);
    }

    @Test
    public void dwarfPerdeTodaVida22Ataques(){
        Dwarf dwarf = new Dwarf("Dain");
        for(int i = 0; i < 22; i++){
            dwarf.sofreDano();
        }
        assertEquals(0.0, dwarf.getVida(),DELTA);
    }

    @Test
    public void dwarfPerdeVidaEContinuaVivo(){
        Dwarf dwarf = new Dwarf("Dain");
        dwarf.sofreDano();
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }

    @Test
    public void dwarfnasceComStatus(){
        Personagem dwarf = new Dwarf("Dain");
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }

    @Test
    public void dwarfEquipaEscudoETomaMetadeDoDano(){
        Dwarf dwarf = new Dwarf("Dain");
        for(int i=0;i<24;i++){
            dwarf.sofreDano();
        }
        assertEquals(Status.MORTO, dwarf.getStatus());
        assertEquals(0.0, dwarf.getVida(),DELTA);
    }

    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Personagem dwarf = new Dwarf("Dain");
        assertEquals("Escudo", dwarf.getInventario().getItens().get(0).getDescricao());
    }

    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf d = new Dwarf("Dain");
        d.getInventario().desequiparItem("Escudo");
        d.sofreDano();
        assertEquals(100,d.getVida(), DELTA);

    }
}
