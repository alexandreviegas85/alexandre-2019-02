

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfBarbaLongaTest
{
    @Test
    public void testarAtaque33Dwarf(){
    DadoFalso d = new DadoFalso();
    d.simularValor(5);
    DwarfBarbaLonga dwarf = new DwarfBarbaLonga("Dwarf",d);
    dwarf.calcularDano();
    assertEquals(100,110, dwarf.getVida());    
    }
}
