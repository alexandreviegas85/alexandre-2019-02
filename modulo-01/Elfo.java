public class Elfo extends Personagem{
    protected int qtdExperiencioPorAtaque = 1 ;
    protected double qtdDano = 0;
    protected int numeroVezesAtaque = 0;
    protected int ganharVidaDuranteAtaque =0;
    protected ExercitoElfo exercito; 
    protected static int qtdElfosCriado;

    public Elfo(String nome){
        super(nome);
        super.ganharItem(new Item(2,"Flecha"));
        super.ganharItem(new Item(1,"Arco"));
        this.vida=100.0;
        Elfo.qtdElfosCriado++;

    }

    protected void finalize()throws Throwable{
        Elfo.qtdElfosCriado--;
        //while(qtdElfosCriado>-0){
          //  qtdElfosCriado--;
       // }
    }
    
    public static int getQtdElfos(){
    return Elfo.qtdElfosCriado;
    }

    public Item getFlecha(){
        return this.getInventario().buscarItem("Flecha");
    }

    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }

    public boolean podeAtirarFlecha(){
        return this.getFlecha().getQuantidade()>0;
    } 

    public void atirarFlechaNoAnao(Dwarf dwarf){
        int qtdAtual = this.getFlecha().getQuantidade();
        if(podeAtirarFlecha() && podeSofrerDano()){
            this.getFlecha().setQuantidade(--qtdAtual);
            this.aumentarXP();
            dwarf.sofreDano();
            this.vida-=this.vida>=calcularDano() ? this.vida-=this.vida-this.calcularDano() : this.vida;
            //this.vida-=calcularDano();
            //this.elfoSofrerDano();
        } 
        if(this.vida == 0){
            this.status = Status.MORTO; 
        }
    }

    public void elfoSofrerDano(){
        podeSofrerDano();
        this.vida -= calcularDano();
    }

    public int aumentarXP(){
        return this.experiencia = this.experiencia + this.qtdExperiencioPorAtaque;
    }

    public double calcularDano(){
        return this.qtdDano;
    }

    public void atacarComEspada(Dwarf dwarf){
        if(podeSofrerDano()){
            this.aumentarXP();
            dwarf.sofreDano();
            this.numeroVezesAtaque++;
        }if(numeroVezesAtaque %2 == 1)
            elfoSofrerDano();
        else 
            this.vida += ganharVidaDuranteAtaque;
        if(this.vida == 0){
            this.status = Status.MORTO; 
        }
    }

    public String imprimirResultado(){
        return "Elfo";
    }



}

