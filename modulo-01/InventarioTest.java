
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest
{   
    @Test
    public void adicionarDoisItens(){
        Inventario inventario= new Inventario(100);
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");

        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);

        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }

    @Test
    public void ObterItemAdicionado(){
        Inventario inventario= new Inventario(100);
        Item espada = new Item(1,"espada");

        inventario.adicionarItem(espada);

        assertEquals(espada, inventario.getItens().get(0));

    }

    @Test
    public void renoverItemAntesDeAdicionarProximo(){
        Inventario inventario= new Inventario(100);

        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");

        inventario.adicionarItem(espada);
        inventario.removerItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals(escudo, inventario.getItens().get(0));
        assertEquals(1, inventario.getItens().size());
    }

    @Test
    public void getDescricoesVariosItens(){
        Inventario inventario= new Inventario(100);

        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");

        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);

        assertEquals("espada,armadura,escudo", inventario.retornarListaNomeItem());

    }

    @Test
    public void getDescricoesNenhumItem() {
        Inventario inventario= new Inventario(100);

        assertEquals("", inventario.retornarListaNomeItem());
    }

    @Test
    public void getItemMaiorQuantidadeComVarios(){
        Inventario inventario= new Inventario(100);

        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");

        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);

        assertEquals(escudo, inventario.retornaItemComMaiorQtd());
    }

    @Test
    public void getItemMaiorQuantidadeComInventarioVazio(){
        Inventario inventario= new Inventario(100);
        assertNull(inventario.retornaItemComMaiorQtd());
    }

    @Test
    public void getItemMaiorQuantidadeComItensMesmaQuantidade(){
        Inventario inventario= new Inventario(100);

        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");

        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);

        assertEquals(espada, inventario.retornaItemComMaiorQtd());
    }

    @Test
    public void acharNomeDeItem(){
        Inventario inventario= new Inventario(100);

        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");

        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);

        assertEquals(espada, inventario.buscarItem(espada.getDescricao()));
    }

    @Test
    public void exibirListaInvertida(){
        Inventario inventario= new Inventario(100);

        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");

        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);

        assertEquals("escudo,armadura,espada", inventario.retornarListaNomeInvertida());
    }

    @Test
    public void testarMetodoOrdenacaoTipoOrdenacaoDesc(){
        Inventario inventario= new Inventario(100);

        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");

        inventario.adicionarItem(bracelete);
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(flecha);

        inventario.ordenarItens(TipoOrdenacao.DESC);

        assertEquals(8, inventario.getItens().get(0).getQuantidade());
        assertEquals(4, inventario.getItens().get(1).getQuantidade());
        assertEquals(3, inventario.getItens().get(2).getQuantidade());
        assertEquals(2, inventario.getItens().get(3).getQuantidade());
        assertEquals(1, inventario.getItens().get(4).getQuantidade());

    }

    @Test
    public void testarMetodoOrdenacaoTipoOrdenacaoAsc(){
        Inventario inventario= new Inventario(100) ;

        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");

        inventario.adicionarItem(bracelete);
        inventario.adicionarItem(espada);
        inventario.adicionarItem(armadura);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(flecha);

        inventario.ordenarItens(TipoOrdenacao.ASC);

        assertEquals(1, inventario.getItens().get(0).getQuantidade());
        assertEquals(2, inventario.getItens().get(1).getQuantidade());
        assertEquals(3, inventario.getItens().get(2).getQuantidade());
        assertEquals(4, inventario.getItens().get(3).getQuantidade());
        assertEquals(8, inventario.getItens().get(4).getQuantidade());

    }

    @Test
    public void buscarItemComInventarioVazio(){
        Inventario inventario= new Inventario(0);     
        assertNull(inventario.buscarItem("capa"));
    }

    @Test
    public void buscarApenasUmItem(){
        Inventario inventario= new Inventario(1);
        Item termica = new Item(1,"Termica de cafe");
        inventario.adicionarItem(termica);
        assertEquals(termica, inventario.buscarItem("Termica de cafe"));
    }

    @Test
    public void buscarApenasUmItemComMesmaDescricao(){
        Inventario inventario= new Inventario(1);
        Item termica1 = new Item(1,"Termica de cafe");
        Item termica2 = new Item(1,"Termica de cafe");
        inventario.adicionarItem(termica1);
        inventario.adicionarItem(termica2);
        assertEquals(termica1, inventario.buscarItem("Termica de cafe"));
    }

    @Test
    public void buscarListaInvertidaApenasUmItem(){
        Inventario inventario= new Inventario(2);
        Item termica1 = new Item(1,"Termica de cafe");

        inventario.adicionarItem(termica1);

        assertEquals(termica1, inventario.inverter().get(0));
    }

    @Test
    public void buscarListaInvertidaComDoisItem(){
        Inventario inventario= new Inventario(2);

        Item termica1 = new Item(1,"Termica de cafe");
        Item termica2 = new Item(2,"Termica de cha");
        inventario.adicionarItem(termica1);
        inventario.adicionarItem(termica2);

        assertEquals(termica2, inventario.inverter().get(0));
        assertEquals(2, inventario.inverter().size());

    }

    @Test
    public void buscarListaInvertidaVazia(){
        Inventario inventario= new Inventario(2);
        assertTrue(inventario.inverter().isEmpty());
    }

    @Test
    public void ganharItens(){
        Inventario inventario= new Inventario(100);
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");

        inventario.ganharItem(espada);
        inventario.ganharItem(escudo);

        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }

    @Test
    public void ganharEperderItens(){
        Inventario inventario= new Inventario(100);
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");

        inventario.ganharItem(espada);
        inventario.ganharItem(escudo);
        inventario.perderItem(espada);
        inventario.perderItem(escudo);

        assertEquals(0,inventario.getItens().size());

    }

    @Test
    public void testarDesequiparItem(){
        Inventario inventario = new Inventario(100);
        Item escudo = new Item(1,"Escudo");
        Item espada = new Item(1,"Espada");
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(espada);
        inventario.desequiparItem("Escudo");
        inventario.desequiparItem("Espada");
        assertEquals(0,inventario.getItens().size());

    }

    @Test
    public void testarEquiparItem(){
        Inventario inventario = new Inventario(100);
        Item escudo = new Item(1,"Escudo");
        Item espada = new Item(1,"Espada");
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(espada);
        inventario.desequiparItem("Escudo");
        inventario.desequiparItem("Espada");
        inventario.equiparItem("Escudo");
        inventario.equiparItem("Espada");
        assertEquals(2,inventario.getItens().size());
    }

}
