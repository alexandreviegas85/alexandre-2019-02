
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticaInventarioTest
{
    @Test
    public void calcularMediaInventarioVazio(){
        Inventario inventario = new Inventario(1);
        EstatisticaInventario estatistica = new EstatisticaInventario(inventario);
        assertTrue(Double.isNaN(estatistica.calcularMedia()));
    }

    @Test
    public void calcularMediaInventarioComUmItem(){
        Inventario inventario = new Inventario(1);
        inventario.adicionarItem(new Item(2,"Escudo"));

        EstatisticaInventario estatistica = new EstatisticaInventario(inventario);
        assertEquals(2, estatistica.calcularMedia(), 1e-9);
    }

    @Test
    public void calcularMediaInventarioComDoisItem(){
        Inventario inventario = new Inventario(1);
        inventario.adicionarItem(new Item(2,"Escudo"));
        inventario.adicionarItem(new Item(2,"Espada"));
        EstatisticaInventario estatistica = new EstatisticaInventario(inventario);
        assertEquals(2, estatistica.calcularMedia(), 1e-9);
    }
    
    @Test
    public void calcularMedianaImpar(){
        Inventario inventario = new Inventario(1);
        inventario.adicionarItem(new Item(2,"Escudo"));
        inventario.adicionarItem(new Item(1,"Espada"));
        inventario.adicionarItem(new Item(3,"Faca"));
        EstatisticaInventario estatistica = new EstatisticaInventario(inventario);
        assertEquals(2, estatistica.calcularMediana(), 1e-9);
    }
    
    @Test
    public void calcularMedianaPar(){
        Inventario inventario = new Inventario(1);
        inventario.adicionarItem(new Item(2,"Escudo"));
        inventario.adicionarItem(new Item(1,"Espada"));
        inventario.adicionarItem(new Item(3,"Faca"));
        inventario.adicionarItem(new Item(3,"Magia"));
        EstatisticaInventario estatistica = new EstatisticaInventario(inventario);
        assertEquals(3, estatistica.calcularMediana(), 1e-9);
    }
}

