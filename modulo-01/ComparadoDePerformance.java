import java.util.*;

public class ComparadoDePerformance
{
    public void comparar(){
        ArrayList<Elfo> arrayElfos = new ArrayList<>();
        HashMap<String, Elfo> hashMapElfo = new HashMap<>();
        int qtdElfo = 1000000;

        for(int i=0;i<qtdElfo;i++){
            String nome = "Elfo "+ i;
            Elfo elfo = new Elfo("Qualquer");
            arrayElfos.add(elfo);
            hashMapElfo.put("Qualquer", elfo);
        }

        String nomeBusca = "Elfo 10000";

        long mSeqInicio = System.currentTimeMillis();
        Elfo elfoSeq = buscaSequencial(arrayElfos, nomeBusca);
        long mSeqFim = System.currentTimeMillis();

        long mHashInicio = System.currentTimeMillis();
        Elfo elfoMap = buscaMap(hashMapElfo, nomeBusca);
        long mHashFim = System.currentTimeMillis();

        String tempoSeq = String.format("%.10f", (mSeqInicio - mSeqFim)/1000.0);
        String tempoHash = String.format("%.10f", (mHashInicio - mHashFim)/1000.0);

        System.out.println("ArrayList: " + tempoSeq);
        System.out.println("HashMap: " + tempoHash);
    }

    private Elfo buscaSequencial(ArrayList<Elfo>  lista, String nome){
        for(Elfo elfo: lista){
            if (elfo.getNome().equals(nome))
                return elfo;
         }
        return null;
    }

    private Elfo buscaMap(HashMap<String, Elfo>  lista, String nome){
        return lista.get(nome);
    }

}
