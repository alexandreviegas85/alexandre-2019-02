
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoElfoTest
{
    private final double DELTA = 1e-9; 

    @Test
    public void alistarElfo(){
        ElfoNoturno elfo = new ElfoNoturno("Elfo");
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfo);
        assertEquals("Elfo" , exercito.getExercito().get(0).getNome());
    } 
    
    @Test
    public void alistarElfoDiferenteDeElfoNoturo(){
        Elfo elfo = new Elfo("Elfo");
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfo);
        assertNull(exercito.buscar(Status.RECEM_CRIADO));
    } 
    
    @Test
    public void getExercito(){
        ElfoNoturno elfo = new ElfoNoturno("Elfo");
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfo);
        assertEquals("Elfo", exercito.getExercito().get(0).getNome());
    } 
    
    @Test
    public void alistarElfoNoturo(){
        ElfoNoturno elfo = new ElfoNoturno("Elfo");
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfo);
        assertEquals(1, exercito.buscar(Status.RECEM_CRIADO).size());
    } 
    
   
    
    
}
