
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest
{
    @Test
    public void adicionarContatoEPesquisar(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Alex", "333");
        agenda.adicionar("Joao", "222");
        agenda.adicionar("Mario", "111");

        assertEquals("333", agenda.consultar("Alex"));
        assertEquals("111",agenda.consultar("Mario"));
        assertEquals("Joao",agenda.consultarNomePorTelefone("222"));
    }

    public void adicionarContatoGerarCVS(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Alex", "333");
        agenda.adicionar("Joao", "222");
       
        String separador = System.lineSeparator();
        String esperado = String.format("Alex,333%sJoao,222%s", separador,separador);
        assertEquals(esperado, agenda.cvs());
        
    }
}
