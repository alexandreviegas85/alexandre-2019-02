import java.util.Random;
public class DwarfBarbaLonga extends Dwarf
{
    private Sorteador sorteador;

    public DwarfBarbaLonga(String nome) {
        super(nome);
        sorteador = new DadoD6();
    }

    public DwarfBarbaLonga(String nome, Sorteador sorteador) {
        super(nome);
        this.sorteador = sorteador;
    }

    public void sofrerDano(){ 
        boolean devePerderVida = sorteador.sortear()<=4;
        if(devePerderVida){
            if(podeSofrerDano()){
                calcularDano();
                if(this.vida ==0.0){
                    this.status = Status.MORTO;
                } 
            }
        }
    }
    /*public boolean calcularProbabilidade(){
    Random gerador = new Random();
    int x = gerador.nextInt(2)+1;
    if(x == 1 || x == 2){ 
    return true;}
    else return false;
    }*/


}
