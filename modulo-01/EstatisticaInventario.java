
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EstatisticaInventario {

    private Inventario inventario;

    public EstatisticaInventario(Inventario inventario){
        this.inventario=inventario;
    }


    // CALCULA MÉDIA DE QTD DE ITENS
    public double calcularMedia() {
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        double soma = 0;
        double media = 0;
        /*
        for(Item item:this.inventario.getItens()){
        soma += inventario.getItens().get(item).getQuantidade();
        }
         */
        for (int i = 0; i < inventario.getItens().size(); i++) {
            soma += inventario.getItens().get(i).getQuantidade();
        }
        media = soma / inventario.getItens().size();
        return media;
    }
    // RETORNA MEDIANA DE ITENS
    public double calcularMediana() {
        if(this.inventario.getItens().isEmpty()){return Double.NaN;}
        inventario.ordenarItens();
        double resultado = 0;
        // SE FOR ÍMPAR
        if (inventario.getItens().size() % 2 != 0) {
            int posicaoNumero = ((inventario.getItens().size() + 1) / 2) - 1;
            resultado = inventario.getItens().get(posicaoNumero).getQuantidade();
        } else {
            // SE FOR PAR
            int posicaoNumero = Math.round(((inventario.getItens().size() + 1) / 2)) - 1;
            resultado = inventario.getItens().get(posicaoNumero).getQuantidade()
            + (inventario.getItens().get(posicaoNumero).getQuantidade() + 1) / 2;
        }
        return resultado;
    }
    
    //CALCULAR QUANTIDADE DE ITENS ACIMA DA MÉDIA
    public int qtdItensAcimaDaMedia() {
        int sum =0;
        double aux = this.calcularMedia();
        for(int i =0; i<inventario.getItens().size();i++) {
            if(inventario.getItens().get(i).getQuantidade()>aux) {
                sum=sum+inventario.getItens().get(i).getQuantidade();
            }
        }
        return sum;
    }
    
    

}
