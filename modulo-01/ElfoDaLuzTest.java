
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    private final double DELTA = 1e-9; 

    @Test
    public void atacarComEspada1VezPerder21Vidas(){
        Dwarf anao = new Dwarf("Dain");
        ElfoDaLuz elfo = new ElfoDaLuz("Elfo");
        elfo.atacarComEspada(anao);
        assertEquals(79.0, elfo.getVida(),DELTA);

    } 

    @Test
    public void atacarComEspada2VezesPerder21VidasGanhar10Vidas(){
        Dwarf anao = new Dwarf("Dain");
        ElfoDaLuz elfo = new ElfoDaLuz("Elfo");
        elfo.atacarComEspada(anao);
        elfo.atacarComEspada(anao);      
        assertEquals(89.0, elfo.getVida(),DELTA);

    }  
}
