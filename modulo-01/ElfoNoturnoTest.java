
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest 
{
    private final double DELTA = 1e-9; 

    @Test
    public void atirarFlechaGanharXPPerderVida(){
        Dwarf novoAnao = new Dwarf("Dain");
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        novoElfo.atirarFlechaNoAnao(novoAnao);
        assertEquals(3,novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getFlecha().getQuantidade());
        assertEquals(85,100, novoElfo.getVida());
    }

    @Test
    public void atirarFlechaEPerder15(){
        Dwarf novoAnao = new Dwarf("Dain");
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        novoElfo.atirarFlechaNoAnao(novoAnao);
        assertEquals(3,novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getFlecha().getQuantidade());
        assertEquals(85.0, novoElfo.getVida(),DELTA );
    }

    @Test
    public void atirarFlecha7FlechasEMorrer(){
        Dwarf novoAnao = new Dwarf("Dain");
        ElfoNoturno elfo = new ElfoNoturno("Legolas");
        elfo.getFlecha().setQuantidade(1000);
        elfo.atirarFlechaNoAnao(novoAnao);
        elfo.atirarFlechaNoAnao(novoAnao);
        elfo.atirarFlechaNoAnao(novoAnao);
        elfo.atirarFlechaNoAnao(novoAnao);
        elfo.atirarFlechaNoAnao(novoAnao);
        elfo.atirarFlechaNoAnao(novoAnao);
        elfo.atirarFlechaNoAnao(novoAnao);
        //assertEquals(0, elfo.getVida(),DELTA);
        assertEquals(Status.MORTO, elfo.getStatus() );
    }

}
